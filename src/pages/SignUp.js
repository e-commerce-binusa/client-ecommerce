import React, { useState } from "react";
import Final from "./form/Final";
import StepOne from "./form/StepOne";
import StepTwo from "./form/StepTwo";

export default function SignUp() {
  const [step, setstep] = useState(1);
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [image, setImage] = useState(null);
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const nextStep = () => {
    setstep(step + 1);
  };

  const prevStep = () => {
    setstep(step - 1);
  };

  switch (step) {
    case 1:
      return (
        <StepOne
          nextStep={nextStep}
          username={username}
          email={email}
          password={password}
          setUsername={setUsername}
          setEmail={setEmail}
          setPassword={setPassword}
        />
      );
    case 2:
      return (
        <StepTwo
          nextStep={nextStep}
          prevStep={prevStep}
          name={name}
          phoneNumber={phoneNumber}
          address={address}
          setName={setName}
          setPhoneNumber={setPhoneNumber}
          setAddress={setAddress}
        />
      );
    case 3:
      return (
        <Final
          username={username}
          email={email}
          password={password}
          name={name}
          image={image}
          phoneNumber={phoneNumber}
          address={address}
          setUsername={setUsername}
          setEmail={setEmail}
          setPassword={setPassword}
          setName={setName}
          setImage={setImage}
          setPhoneNumber={setPhoneNumber}
          setAddress={setAddress}
        />
      );
    default:
      return <div className="container"></div>;
  }
}
