import React from "react";
import Sidebar from "../../component/Sidebar";
import "../../assets/Category.css";
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import $ from "jquery";
import { useState } from "react";
import axios from "axios";
import { API_USER } from "../../util/api";
import { useEffect } from "react";

export default function Category() {
  const [user, setUser] = useState([]);

  const getAllUser = async () => {
    await axios
      .get(`${API_USER}/role`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  $(document).ready(function () {
    setTimeout(function () {
      $("#tabel").DataTable();
    }, 500);
  });

  useEffect(() => {
    getAllUser();
  }, []);
  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
        <Sidebar />
      </div>
      {/* Content */}
      <main id="content" className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0 left-0">
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="header font-medium text-xl text-white">
              <span>DAFTAR PENGGUNA</span>
            </div>
            <div className="overflow-x-auto overflow-y-auto lg:max-w-full md:max-w-lg max-w-md mt-5 bg-white p-5 rounded-md">
              <table
                className="min-w-full text-xs text-black text-center py-3 row-border hover"
                id="tabel"
              >
                <thead className="border-b border-opacity-20 bg-white sticky top-0">
                  <tr>
                    <th className="p-3">Id</th>
                    <th className="p-3">Avatar</th>
                    <th className="p-3">Username</th>
                    <th className="p-3">Nama Lengkap</th>
                    <th className="p-3">Email</th>
                    {/* <th className="p-3">Alamat</th> */}
                    <th className="p-3">No.Telp</th>
                  </tr>
                </thead>
                <tbody className="bg-white">
                  {user.map((users, idx) => (
                    <tr
                      key={users.id}
                      className="border-b border-opacity-20 text-sm border-black"
                    >
                      <td className="p-3">
                        <p>{idx + 1}</p>
                      </td>
                      <td className="p-3">
                        <img
                          className="w-[60px] max-h-[60px]"
                          src={users.image}
                          alt="user"
                        />
                      </td>
                      <td className="p-3">
                        <p>{users.username}</p>
                      </td>
                      <td className="p-3">
                        <p>{users.name}</p>
                      </td>
                      <td className="p-3">
                        <p>{users.email}</p>
                      </td>
                      {/* <td className="p-3">
                        <p>{users.address}</p>
                      </td> */}
                      <td className="p-3">
                        <p>{users.phoneNumber}</p>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </main>
    </div>
  );
}
