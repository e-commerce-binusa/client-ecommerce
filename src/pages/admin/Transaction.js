import React from "react";
import Sidebar from "../../component/Sidebar";
import $ from "jquery";
import { useState } from "react";
import axios from "axios";
import { API_ORDER } from "../../util/api";
import { useEffect } from "react";
import { titik } from "../../util/Rp";
import Moment from "react-moment";

export default function Transaction() {
  const [pesanan, setPesanan] = useState([]);

  const getPesanan = async () => {
    await axios
      .get(`${API_ORDER}/list`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setPesanan(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  $(document).ready(function () {
    setTimeout(function () {
      $("#tabel").DataTable();
    }, 500);
  });

  useEffect(() => {
    getPesanan();
  }, []);

  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
        <Sidebar />
      </div>
      {/* Content */}
      <main
        id="content"
        className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0 left-0"
      >
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="header font-medium text-xl text-white mb-4">
              <span>TRANSAKSI</span>
            </div>
            <div className="overflow-x-auto overflow-y-auto lg:max-w-full md:max-w-lg max-w-md mt-5 bg-white p-5 rounded-md">
              <table
                className="min-w-full text-xs text-black py-3 row-border hover"
                id="tabel"
              >
                <thead className="border-b border-opacity-20 bg-white sticky top-0">
                  <tr>
                    <th className="p-3 text-[16px]">Id</th>
                    <th className="p-3 text-[16px]">Nama Pengguna</th>
                    <th className="p-3 text-[16px]">Nama Produk</th>
                    <th className="p-3 text-[16px]">Harga Satuan</th>
                    <th className="p-3 text-[16px]">Quantity</th>
                    <th className="p-3 text-[16px]">Total Harga</th>
                    <th className="p-3 text-[16px]">Tanggal Pesan</th>
                    <th className="p-3 text-[16px]">Status</th>
                    <th className="p-3 text-[16px]">Aksi</th>
                  </tr>
                </thead>
                <tbody className="bg-white">
                  {pesanan.map((pesanans, idx) => (
                    <tr className="border-b border-opacity-20 border-gray-700 font-semibold text-sm">
                      <td className="p-3">
                        <p>{idx + 1}</p>
                      </td>
                      <td className="p-3">
                        <p>{pesanans.user.name}</p>
                      </td>
                      <td className="p-3">
                        <p>{pesanans.product.name}</p>
                      </td>
                      <td className="p-3">
                        <p>{titik(pesanans.product.price)}</p>
                      </td>
                      <td className="p-3">
                        <p>{pesanans.totalProduct}</p>
                      </td>
                      <td className="p-3">
                        <p>{titik(pesanans.totalPrice)}</p>
                      </td>
                      <td className="p-3">
                        <p>
                          <Moment format="DD/MM/YYYY">
                            {pesanans.createdAt}
                          </Moment>
                        </p>
                      </td>
                      <td className="p-3">
                        {pesanans.process === true ? (
                          <p className="text-green-500">Selesai Diproses</p>
                        ) : (
                          <p className="text-yellow-300">Sedang Diproses</p>
                        )}
                      </td>
                      <td className="p-3">
                        <a href={"/admin/pesanan/" + pesanans.id}>
                          <button
                            type="button"
                            className="px-8 py-2 w-[100px] font-semibold rounded-full border border-[#34bc94] bg-[#34bc94] text-white"
                          >
                            Detail
                          </button>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </main>
      {/* Content */}
    </div>
  );
}
