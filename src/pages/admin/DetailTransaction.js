import React from "react";
import Sidebar from "../../component/Sidebar";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { API_ORDER } from "../../util/api";
import { useEffect } from "react";
import Moment from "react-moment";
import { titik } from "../../util/Rp";
import Swal from "sweetalert2";

export default function DetailTransaction() {
  const [pesanan, setPesanan] = useState({
    message: "",
    totalProduct: 0,
    totalPrice: 0,
    createdAt: null,
    user: {},
    product: {},
    process: null,
  });

  const { id } = useParams();
  const navigate = useNavigate();

  const getPesananId = async () => {
    await axios
      .get(`${API_ORDER}/list/${id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setPesanan(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const process = async () => {
    await axios
      .put(
        `${API_ORDER}/process/${id}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      )
      .then(() => {
        navigate("/admin/transaction");
        Swal.fire({
          icon: "success",
          title: "Pesanan telah diproses!",
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getPesananId();
  }, []);
  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
        <Sidebar />
      </div>
      {/* Content */}
      <main
        id="content"
        className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0 md left-0"
      >
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="header font-medium text-xl text-white mb-4">
              <span>Detail Transaksi</span>
            </div>
            <div className="overflow-x-auto overflow-y-auto lg:max-w-full md:max-w-lg max-w-md mt-5 bg-white rounded-md">
              <div className="p-5 space-y-2">
                <div className="space-y-2">
                  <div className="flex space-x-2">
                    <p className="font-semibold">{pesanan.user.name}</p>
                    <span className="bg-gray-400 w-[0.1px]"></span>
                    <p>{pesanan.user.phoneNumber}</p>
                  </div>
                  <p>{pesanan.user.address}</p>
                </div>
                <hr />
                <div className="grid grid-cols-1 md:grid-cols-3 gap-2">
                  <div className="bg-white rounded-lg flex justify-center p-2">
                    <img
                      className="w-full lg:h-80 md:h-56 h-48"
                      src={pesanan.product.image}
                      alt="product_image"
                    />
                  </div>
                  <div className="col-span-2 bg-white rounded-lg p-2">
                    <div>
                      <span className="font-bold text-2xl text-gray-600">
                        {pesanan.product.name}
                      </span>
                      {pesanan.process === true ? (
                        <button className="float-right bg-[#8bd3c0] text-white font-bold py-2 px-4 rounded cursor-not-allowed">
                          Selesai Diproses
                        </button>
                      ) : (
                        <button
                          onClick={process}
                          className="float-right rounded px-5 py-2 bg-yellow-300 text-black"
                        >
                          Sedang Diproses
                        </button>
                      )}
                    </div>
                    <div className="mt-6">
                      <span className="font-bold text-lg text-gray-600">
                        Catatan
                      </span>
                      <hr className="w-[70px] border border-gray-600" />
                    </div>
                    <div className="mt-2 lg:max-h-[100px] lg:h-[100px] md:max-h-[80px] md:h-[80px] max-h-32 h-32 overflow-y-scroll cut text-justify">
                      <span className="text-sm break-words">{pesanan.message}</span>
                    </div>
                    <div className="flex space-x-3 my-3">
                      <p className="font-semibold">Quantity :</p>
                      <p>{pesanan.totalProduct}</p>
                    </div>{" "}
                    <div className="flex space-x-3 my-3">
                      <p className="font-semibold">Total Harga :</p>
                      <p>{titik(pesanan.totalPrice)}</p>
                    </div>{" "}
                    <div>
                      <div className="flex space-x-3">
                        <p className="font-semibold">Tanggal Pesan :</p>
                        <p>
                          <Moment format="DD/MM/YYYY">
                            {pesanan.createdAt}
                          </Moment>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      {/* Content */}
    </div>
  );
}
