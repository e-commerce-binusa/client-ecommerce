import React, { useState } from "react";
import Sidebar from "../../component/Sidebar";
import "../../assets/Category.css";
import { API_CATEGORY } from "../../util/api";
import axios from "axios";
import { useEffect } from "react";
import Swal from "sweetalert2";
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import $ from "jquery";

export default function Category() {
  const [showModalAdd, setShowModalAdd] = useState(false);
  const [showModalEdit, setShowModalEdit] = useState(false);
  const [name, setName] = useState("");
  const [category, setCategory] = useState([]);
  const [nameId, setNameId] = useState("");
  const [categoryId, setCategoryId] = useState(0);

  $(document).ready(function () {
    setTimeout(function () {
      $("#tabel").DataTable();
    }, 500);
  });

  const getAllCategory = async () => {
    await axios
      .get(API_CATEGORY + "/all")
      .then((res) => {
        setCategory(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getCategoryId = async (id) => {
    await axios
      .get(API_CATEGORY + "/id/" + id)
      .then((res) => {
        setNameId(res.data.name);
        setCategoryId(id);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const addCategory = async (e) => {
    e.preventDefault();
    const req = {
      name: name,
    };
    await Swal.fire({
      title: "Anda Yakin?",
      text: "Menambahkan Kategori",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, tambahkan!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(API_CATEGORY + "/add", req, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            setShowModalAdd(false);
            getAllCategory();
            Swal.fire({
              icon: 'success',
              title: 'Kategori Ditambahkan!',
              showConfirmButton: false,
              timer: 1500
            })
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  const updateCategory = async (e) => {
    e.preventDefault();
    const req = {
      name: nameId,
    };
    await Swal.fire({
      title: "Anda Yakin?",
      text: "Mengupdate Kategori",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, ubah!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(API_CATEGORY + "/update/" + categoryId, req, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            setShowModalEdit(false);
            getAllCategory();
            Swal.fire({
              icon: 'success',
              title: 'Kategori Diubah!',
              showConfirmButton: false,
              timer: 1500
            })
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  const deleteCategory = async (id) => {
    await Swal.fire({
      title: "Anda Yakin?",
      text: "Kategori yang dipilih akah terhapus!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .delete(API_CATEGORY + "/delete/" + id, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            getAllCategory();
            Swal.fire({
              icon: 'success',
              title: 'Kategori Dihapus!',
              showConfirmButton: false,
              timer: 1500
            })
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  useEffect(() => {
    getAllCategory();
    getCategoryId();
  }, []);
  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
        <Sidebar />
      </div>
      {/* Content */}
      <main id="content" className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0">
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="grid grid-cols-2">
              <div className="header font-medium text-xl text-white">
                <span>KATEGORI</span>
              </div>
              <div>
                <span
                  onClick={() => {
                    setShowModalAdd(true);
                  }}
                  className="border-2 border-[#34bc94] bg-white font-medium text-[#34bc94] cursor-pointer shadow w-44 px-4 py-2 text-center text-sm rounded-full float-right"
                >
                  Tambah Kategori
                </span>
              </div>
            </div>
            <div className="overflow-x-auto overflow-y-auto lg:max-w-full md:max-w-lg max-w-md mt-5 bg-white p-5 rounded-md">
              <table
                className="min-w-full text-xs text-black text-center py-3 row-border hover"
                id="tabel"
              >
                <thead className="border-b border-opacity-20 bg-white sticky top-0">
                  <tr>
                    <th className="p-3 text-[16px]">Id</th>
                    <th className="p-3 text-[16px]">Nama Kategori</th>
                    <th className="p-3 text-[16px]">Aksi</th>
                  </tr>
                </thead>
                <tbody className="bg-white">
                  {category.map((categories, idx) => (
                    <tr className="border-b border-opacity-20 border-gray-700 font-semibold text-sm">
                      <td className="p-3">
                        <p>{idx + 1}</p>
                      </td>
                      <td className="p-3">
                        <p>{categories.name}</p>
                      </td>
                      <td className="p-3 text-center">
                        <div>
                          <button
                            onClick={() => {
                              setShowModalEdit(true);
                              getCategoryId(categories.id);
                            }}
                            type="button"
                            className="mx-2 my-0.5 px-8 py-2 w-[100px] font-semibold rounded-full border border-[#34bc94] bg-[#34bc94] text-white"
                          >
                            Edit
                          </button>
                          <button
                            type="button"
                            onClick={() => deleteCategory(categories.id)}
                            className="mx-2 my-0.5 px-8 py-2 w-[100px] font-semibold rounded-full border border-white bg-[#34bc94] text-white hover:bg-red-500"
                          >
                            Hapus
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </main>

      {/* MODAL ADD */}
      {showModalAdd ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Tambah Kategori
                </h2>
                <form className="space-y-4" onSubmit={addCategory}>
                  <div>
                    <label className="sr-only" htmlFor="name">
                      Nama Kategori
                    </label>
                    <input
                      className="w-full placeholder:text-[#34bc94] focus:ring-[#13ac84] focus:outline-none focus:border-green-400 rounded-lg border border-[#13ac84] font-semibold p-3 text-sm"
                      placeholder="Nama Kategori"
                      type="text"
                      id="name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                      autoComplete="off"
                      required
                    />
                  </div>
                  <div className="flex flex-col justify-end gap-3 mt-6 sm:flex-row">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalAdd(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

      {/* MODAL EDIT */}
      {showModalEdit ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Edit Kategori
                </h2>
                <form className="space-y-4" onSubmit={updateCategory}>
                  <div>
                    <label className="sr-only" htmlFor="name">
                      Nama Kategori
                    </label>
                    <input
                      className="w-full placeholder:text-[#34bc94] rounded-lg border border-[#34bc94] font-semibold p-3 text-sm"
                      placeholder="Nama Kategori"
                      type="text"
                      id="name"
                      value={nameId}
                      autoComplete="off"
                      onChange={(e) => setNameId(e.target.value)}
                    />
                  </div>
                  <div className="flex flex-col justify-end gap-3 mt-6 sm:flex-row">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalEdit(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </div>
  );
}
