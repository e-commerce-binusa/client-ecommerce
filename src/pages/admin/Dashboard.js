import React, { useEffect, useState } from "react";
import Sidebar from "../../component/Sidebar";
import "../../assets/Dashboard.css";
import { API_CATEGORY, API_PRODUCT, API_USER } from "../../util/api";
import axios from "axios";

export default function Dashboard() {
  const [product, setProduct] = useState([]);
  const [category, setCategory] = useState([]);
  const [user, setUser] = useState([]);

  const getAllProduct = async () => {
    await axios
      .get(API_PRODUCT + "/all")
      .then((res) => {
        setProduct(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllUser = async () => {
    await axios
      .get(API_USER + "/role", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllCategory = async () => {
    await axios
      .get(API_CATEGORY + "/all")
      .then((res) => {
        setCategory(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getAllProduct();
    getAllUser();
    getAllCategory();
  }, []);

  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
      <Sidebar />
      </div>
      {/* Content */}
      <main id="content" className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0 md left-0">
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="header font-medium text-xl text-white mb-4">
              <span>DASHBOARD</span>
            </div>
            <div className="container grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 md:gap-3 lg:gap-6 mx-auto md:mx-0 lg:mx-auto">
              <a href="/admin/product" className="flex p-4 md:p-2 lg:p-4 space-x-4 rounded-lg md:space-x-2 lg:space-x-6 bg-white text-black cursor-pointer">
                <div className="flex justify-center p-2 md:p-3 lg:p-4 align-middle rounded-lg bg-[#34bc94]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                    fill="currentColor"
                    className="h-9 w-9 md:h-8 md:w-8 lg:h-9 lg:w-9 text-gray-800"
                  >
                    <polygon points="160 96.039 160 128.039 464 128.039 464 191.384 428.5 304.039 149.932 304.039 109.932 16 16 16 16 48 82.068 48 122.068 336.039 451.968 336.039 496 196.306 496 96.039 160 96.039"></polygon>
                    <path d="M176.984,368.344a64.073,64.073,0,0,0-64,64h0a64,64,0,0,0,128,0h0A64.072,64.072,0,0,0,176.984,368.344Zm0,96a32,32,0,1,1,32-32A32.038,32.038,0,0,1,176.984,464.344Z"></path>
                    <path d="M400.984,368.344a64.073,64.073,0,0,0-64,64h0a64,64,0,0,0,128,0h0A64.072,64.072,0,0,0,400.984,368.344Zm0,96a32,32,0,1,1,32-32A32.038,32.038,0,0,1,400.984,464.344Z"></path>
                  </svg>
                </div>
                <div className="flex flex-col justify-center align-middle">
                  <p className="text-3xl font-semibold leading-none">
                    { product.length } PRODUK
                  </p>
                </div>
              </a>
              <a href="/admin/list-user" className="flex p-4 md:p-2 lg:p-4 space-x-4 rounded-lg md:space-x-2 lg:space-x-6 bg-white text-black cursor-pointer">
                <div className="flex justify-center p-2 md:p-3 lg:p-4 align-middle rounded-lg sm:p-4 bg-[#34bc94]">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-9 w-9 md:h-8 md:w-8 lg:h-9 lg:w-9 text-white"
                    viewBox="0 0 448 512"
                  >
                    <path d="M272 304h-96C78.8 304 0 382.8 0 480c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32C448 382.8 369.2 304 272 304zM48.99 464C56.89 400.9 110.8 352 176 352h96c65.16 0 119.1 48.95 127 112H48.99zM224 256c70.69 0 128-57.31 128-128c0-70.69-57.31-128-128-128S96 57.31 96 128C96 198.7 153.3 256 224 256zM224 48c44.11 0 80 35.89 80 80c0 44.11-35.89 80-80 80S144 172.1 144 128C144 83.89 179.9 48 224 48z" />
                  </svg>
                </div>
                <div className="flex flex-col justify-center align-middle">
                  <p className="text-3xl font-semibold leading-none">
                    {user.length} Pengguna
                  </p>
                </div>
              </a>
              <a href="/admin/category"  className="flex p-4 md:p-2 lg:p-4 space-x-4 rounded-lg md:space-x-2 lg:space-x-6 bg-white text-black cursor-pointer">
                <div className="flex justify-center p-2 md:p-3 lg:p-4 align-middle rounded-lg sm:p-4 bg-[#34bc94]">
                  <svg
                    className="h-9 w-9 md:h-8 md:w-8 lg:h-9 lg:w-9 text-gray-800"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                  >
                    <path d="M40 48C26.7 48 16 58.7 16 72v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V72c0-13.3-10.7-24-24-24H40zM192 64c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zM16 232v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V232c0-13.3-10.7-24-24-24H40c-13.3 0-24 10.7-24 24zM40 368c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V392c0-13.3-10.7-24-24-24H40z" />
                  </svg>
                </div>
                <div className="flex flex-col justify-center align-middle">
                  <p className="text-3xl font-semibold leading-none">
                    { category.length } KATEGORI
                  </p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </main>
      {/* Content */}
    </div>
  );
}
