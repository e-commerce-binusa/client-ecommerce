import React, { useEffect, useState } from "react";
import Sidebar from "../../component/Sidebar";
import "../../assets/product-admin.css";
import { API_PRODUCT, API_CATEGORY } from "../../util/api";
import axios from "axios";
import Swal from "sweetalert2";
import { titik } from "../../util/Rp";
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import $ from "jquery";
import { storage } from "../../firebase";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";

export default function Product() {
  const [showModalAdd, setShowModalAdd] = useState(false);
  const [showModalEdit, setShowModalEdit] = useState(false);
  const [productId, setProductId] = useState(0);
  const [image, setImage] = useState(null);
  const [name, setName] = useState("");
  const [stock, setStock] = useState(0);
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [categoryId, setCategoryId] = useState(0);
  const [category, setCategory] = useState([]);
  const [product, setProduct] = useState([]);
  const [arr, setArr] = useState([]);

  $(document).ready(function () {
    setTimeout(function () {
      $("#tabel").DataTable();
    }, 500);
  });

  const getAllCategory = async () => {
    await axios
      .get(API_CATEGORY + "/all")
      .then((res) => {
        setCategory(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllProduct = async () => {
    await axios
      .get(API_PRODUCT + "/all")
      .then((res) => {
        setProduct(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getProductId = async (id) => {
    await axios
      .get(API_PRODUCT + "/id/" + id)
      .then((res) => {
        setName(res.data.name);
        setImage(res.data.image);
        setDescription(res.data.description);
        setStock(res.data.stock);
        setPrice(res.data.price);
        setProductId(id);
        setArr(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const postProduct = async (url) => {
    try {
      const req = {
        name: name,
        image: url,
        description: description,
        stock: stock,
        price: price,
        categoryId: categoryId,
      };
      await Swal.fire({
        title: "Anda Yakin?",
        text: "Menambahkan Product",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, tambahkan!",
      }).then((result) => {
        if (result.isConfirmed) {
          axios
            .post(API_PRODUCT + "/add", req, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then(() => {
              setShowModalAdd(false);
              getAllProduct();
              Swal.fire({
                icon: "success",
                title: "Product Ditambahkan!",
                showConfirmButton: false,
                timer: 1500,
              });
            })
            .catch(() => {
              Swal.fire({
                icon: "error",
                title: "Terjadi Kesalahan!",
                showConfirmButton: false,
                timer: 1500,
              });
            });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    const storageRef = ref(storage, `product/${image.name}`);

    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload Berhasil!!");
        console.log(snapshot);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          postProduct(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  const updateHandler = async (url) => {
    const req = {
      name: name,
      image: url,
      description: description,
      stock: stock,
      price: price,
      categoryId: categoryId,
    };
    await Swal.fire({
      title: "Yakin ingin menyimpan perubahan data produk ini?",
      icon: "question",
      showDenyButton: true,
      confirmButtonText: "Ya, simpan!",
      denyButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put(`${API_PRODUCT}/update/${productId}`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        const storageRef = ref(storage, `product/${arr.image}`);
        deleteObject(storageRef);
        Swal.fire({
          icon: "success",
          title: "Perubahan tersimpan!",
          showConfirmButton: false,
          timer: 1500,
        });
        setShowModalEdit(false);
        getAllProduct();
      } else if (result.isDenied) {
        Swal.fire({
          icon: "info",
          title: "Perubahan tidak disimpan!",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const submitUpdate = () => {
    const storageRef = ref(storage, `product/${image.name}`);

    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          updateHandler(downloadUrl);
        });
      });
  };

  const saveUpdate = (e) => {
    e.preventDefault();
    submitUpdate();
  };

  const deleteProduct = async (id) => {
    await Swal.fire({
      title: "Anda Yakin?",
      text: "Produk yang dipilih akah terhapus!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .delete(API_PRODUCT + "/delete/" + id, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            const storageRef = ref(storage, `product/${arr.image}`);
            deleteObject(storageRef);
            Swal.fire({
              icon: "success",
              title: "Produk dihapus!",
              showConfirmButton: false,
              timer: 1500,
            });
            getAllProduct();
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  useEffect(() => {
    getAllCategory();
    getAllProduct();
  }, []);

  return (
    <div
      className="relative min-h-screen md:flex back"
      data-dev-hint="container"
    >
      <div className="lg:w-auto lg:h-[100vh] lg:fixed lg:top-0 lg:left-0 md:w-auto md:h-[100vh] md:sticky md:top-0 md:left-0 w-full h-[100vh] fixed top-0 left-auto">
        <Sidebar />
      </div>
      {/* Content */}
      <main id="content" className="flex-1 p-6 lg:px-8 lg:ml-64 md:ml-0">
        <div className="max-w-7xl mx-auto lg:mt-0 md:mt-0 mt-10 overflow-x-auto">
          <div className="px-4 py-6 sm:px-0">
            <div className="grid grid-cols-2">
              <div className="header font-medium text-xl text-white">
                <span>PRODUK</span>
              </div>
              <div>
                <span
                  onClick={() => {
                    setShowModalAdd(true);
                  }}
                  className="border-2 border-[#34bc94] bg-white font-medium text-[#34bc94] cursor-pointer shadow w-44 px-4 py-2 text-center text-sm rounded-full float-right"
                >
                  Tambah Produk
                </span>
              </div>
            </div>
            <div className="overflow-x-auto overflow-y-auto lg:max-w-full md:max-w-lg max-w-md mt-5 bg-white p-5 rounded-md">
              <table
                className="min-w-full text-xs text-black text-center font-semibold py-3 row-border hover"
                id="tabel"
              >
                <thead className="border-b border-opacity-20 bg-white sticky top-0">
                  <tr>
                    <th className="p-3 text-[16px]">Id</th>
                    <th className="p-3 text-[16px]">Gambar Produk</th>
                    <th className="p-3 text-[16px]">Nama Produk</th>
                    <th className="p-3 text-[16px]">Stok</th>
                    <th className="p-3 text-[16px]">Harga</th>
                    <th className="p-3 text-[16px]">Aksi</th>
                  </tr>
                </thead>
                <tbody className="bg-white">
                  {product.map((products, idx) => (
                    <tr
                      key={products.id}
                      className="border-b border-opacity-20 text-sm border-black"
                    >
                      <td className="p-3">
                        <p>{idx + 1}</p>
                      </td>
                      <td className="p-3">
                        <img
                          className="w-[120px] max-h-[180px]"
                          src={products.image}
                          alt="product"
                        />
                      </td>
                      <td className="p-3">
                        <p>{products.name}</p>
                      </td>
                      <td className="p-3">
                        <p>{products.stock}</p>
                      </td>
                      <td className="p-3">
                        <p>{titik(products.price)}</p>
                      </td>
                      <td className="p-3 text-center">
                        <div>
                          <button
                            onClick={() => {
                              setShowModalEdit(true);
                              getProductId(products.id);
                            }}
                            type="button"
                            className="mx-2 my-0.5 px-8 py-2 w-[100px] font-semibold rounded-full border border-white bg-[#34bc94] text-white"
                          >
                            Edit
                          </button>
                          <button
                            onClick={() => deleteProduct(products.id)}
                            type="button"
                            className="ml-2 mx-2 my-0.5 px-8 py-2 w-[100px] font-semibold rounded-full border border-white bg-[#34bc94] text-white hover:bg-red-500"
                          >
                            Hapus
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </main>

      {/* MODAL ADD */}
      {showModalAdd ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Tambah Produk
                </h2>
                <form className="space-y-4" onSubmit={save}>
                  <div className="image-product">
                    <div>
                      <label htmlFor="imageProduct">Image Product</label>
                      <input
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        placeholder="gambar"
                        type="file"
                        id="file"
                        onChange={(e) => setImage(e.target.files[0])}
                        autoComplete="off"
                        required
                      />
                    </div>

                    <div>
                      <label htmlFor="name">Nama Produk</label>
                      <input
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        placeholder="Nama Produk"
                        type="text"
                        id="name"
                        onChange={(e) => setName(e.target.value)}
                        autoComplete="off"
                        required
                      />
                    </div>

                    <div className="grid grid-cols-1 gap-4 sm:grid-cols-2">
                      <div>
                        <label htmlFor="stock">Stock</label>
                        <input
                          className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                          placeholder="0"
                          type="number"
                          id="stock"
                          onChange={(e) => setStock(e.target.value)}
                          autoComplete="off"
                          required
                        />
                      </div>

                      <div>
                        <label htmlFor="price">Harga</label>
                        <input
                          className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                          placeholder="0"
                          type="number"
                          id="price"
                          onChange={(e) => setPrice(e.target.value)}
                          autoComplete="off"
                          required
                        />
                      </div>
                    </div>

                    <div className="mt-4">
                      <label htmlFor="description">Category</label>
                      <select
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        id="category"
                        name="category"
                        autoComplete="category-name"
                        onChange={(e) => setCategoryId(e.target.value)}
                        required
                      >
                        <option className="hidden">Category</option>
                        {category.map((categories) => (
                          <option key={categories.id} value={categories.id}>
                            {categories.name}
                          </option>
                        ))}
                      </select>
                    </div>

                    <div>
                      <label htmlFor="description">Deskripsi</label>
                      <textarea
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        placeholder="Deskripsi"
                        rows="4"
                        id="description"
                        onChange={(e) => setDescription(e.target.value)}
                        required
                      ></textarea>
                    </div>
                  </div>
                  <div className="flex flex-col justify-end gap-3 mt-6 sm:flex-row">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalAdd(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

      {/* MODAL EDIT */}
      {showModalEdit ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Edit Produk
                </h2>
                <form onSubmit={saveUpdate} className="space-y-4">
                  <div>
                    <label htmlFor="imageProduct">Image Product</label>
                    <input
                      className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                      placeholder="gambar"
                      type="file"
                      id="image"
                      onChange={(e) => setImage(e.target.files[0])}
                      autoComplete="off"
                      required
                    />
                  </div>
                  <div className="image-product">
                    <div>
                      <label htmlFor="name">Nama Produk</label>
                      <input
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        placeholder="nama produk"
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        autoComplete="off"
                      />
                    </div>

                    <div className="grid grid-cols-1 gap-4 sm:grid-cols-2">
                      <div className="">
                        <label htmlFor="stock">Stock</label>
                        <input
                          className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                          placeholder="stock"
                          type="number"
                          id="stockId"
                          value={stock}
                          onChange={(e) => setStock(e.target.value)}
                          autoComplete="off"
                        />
                      </div>

                      <div>
                        <label htmlFor="price">Harga</label>
                        <input
                          className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                          placeholder="price"
                          type="number"
                          id="priceId"
                          value={price}
                          onChange={(e) => setPrice(e.target.value)}
                          autoComplete="off"
                        />
                      </div>
                    </div>

                    <div>
                      <label htmlFor="description">Deskripsi</label>
                      <textarea
                        className="w-full placeholder:text-[#34bc94] rounded-lg border focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] border-[#34bc94] font-semibold p-3 text-sm"
                        placeholder="deskripsi"
                        rows="4"
                        id="description"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                      ></textarea>
                    </div>
                  </div>
                  <div className="flex flex-col justify-end gap-3 mt-6 sm:flex-row">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalEdit(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </div>
  );
}
