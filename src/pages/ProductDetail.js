import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "../component/Navbar";
import { API_CART, API_PRODUCT } from "../util/api";
import "../assets/product-admin.css";
import { titik } from "../util/Rp";
import Swal from "sweetalert2";
import Footer from "../component/Footer";

export default function ProductDetail() {
  let [count, setCount] = useState(0);
  const [total, setTotal] = useState(0);
  const [product, setProduct] = useState({
    image: null,
    name: "",
    description: "",
    stock: 0,
    price: 0,
  });
  const navigate = useNavigate();
  const { id } = useParams();

  const getProductId = async () => {
    await axios
      .get(API_PRODUCT + "/id/" + id)
      .then((res) => {
        setProduct(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const increment = () => {
    if (count === product.stock) {
      setCount((count = product.stock));
      setTotal(product.price * product.stock);
    } else {
      setCount(count + 1);
      setTotal(product.price * (count + 1));
    }
  };

  const decrement = () => {
    if (count === 0) {
      setCount((count = 0));
      setTotal(total);
    } else {
      setCount(count - 1);
      setTotal(product.price * (count - 1));
    }
  };

  const addToCart = async () => {
    await axios
      .get(
        `${API_CART}/search?product=${id}&user=${localStorage.getItem(
          "userId"
        )}`,
        {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("token"),
          },
        }
      )
      .then((res) => {
        if (res.data.length === 0) {
          const req = {
            productId: id,
            quantity: count,
          };
          axios
            .post(`${API_CART}/add`, req, {
              headers: {
                Authorization: "Bearer " + localStorage.getItem("token"),
              },
            })
            .then(() => {
              Swal.fire({
                icon: "success",
                title: "Produk berhasil ditambahkan!",
                showConfirmButton: false,
                timer: 1500,
              });
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          const req = {
            quantity: res.data[0].quantity + count,
          };
          axios
            .put(`${API_CART}/update/${res.data[0].id}`, req, {
              headers: {
                Authorization: "Bearer " + localStorage.getItem("token"),
              },
            })
            .then(() => {
              Swal.fire({
                icon: "success",
                title: "Produk berhasil ditambahkan!",
                showConfirmButton: false,
                timer: 1500,
              });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const notYetLogin = () => {
    navigate("/login");
  };

  useEffect(() => {
    getProductId();
  }, []);

  return (
    <div className="bg-gray-100">
      <div>
        <Navbar />
      </div>

      <div className="container mx-auto block rounded bg-white border border-gray-100 lg:p-8 md:p-2 p-5 mt-4 mb-4 shadow-xl">
        <div className="grid grid-cols-1 md:grid-cols-3 md:gap-2">
          <div className="bg-white rounded-lg flex justify-center w-full p-2">
            {product.stock !== 0 ? (
              <img
                className="lg:h-96 w-full md:h-80 h-72"
                src={product.image}
                alt="product_image"
              />
            ) : (
              <div className="relative w-full">
                <img
                  src={product.image}
                  alt="stock-habis"
                  className="lg:h-96 w-full md:h-80 h-72"
                />
                <div className="flex justify-center items-center">
                  <div className="absolute flex justify-center items-center h-20 w-20 bg-slate-100 rounded-full lg:bottom-40 md:bottom-32 bottom-24">
                    <span className="text-black">Habis</span>
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className="col-span-2 bg-white rounded-lg p-2">
            <div>
              <span className="font-bold text-2xl text-gray-600">
                {product.name}
              </span>
            </div>
            <div className="lg:my-4 md:my-1 my-2">
              <span className="lg:font-semibold lg:text-3xl md:font-semibold font-semibold md:text-2xl text-2xl text-gray-600">
                {titik(product.price)}
              </span>
            </div>

            {/* quantity and button AddToCart in Mobile UI start */}
            <div className="md:hidden flex items-end gap-2 mt-3">
              <div className="w-fit">
                <label htmlFor="Quantity" className="sr-only">
                  {" "}
                  Quantity{" "}
                </label>

                <div className="flex items-center border border-gray-200 rounded">
                  <button
                    type="button"
                    onClick={decrement}
                    className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                  >
                    &minus;
                  </button>

                  <input
                    type="number"
                    id="Quantity"
                    value={count}
                    className="h-10 w-16 border-transparent text-center focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                    autoComplete="off"
                  />

                  <button
                    onClick={increment}
                    type="button"
                    className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                  >
                    +
                  </button>
                </div>
              </div>
              <div className="w-full flex justify-end">
                <div className="mb-2">
                  <span>Stok: {product.stock}</span>
                </div>
              </div>
            </div>
            {/* quantity and button AddToCart in Mobile UI end */}

            <div className="mt-3 md:mt-0 lg:mt-0">
              <span className="font-bold text-xl text-gray-600">Deskripsi</span>
              <hr className="w-[90px] border-2 border-gray-600" />
            </div>
            <div className="mt-2 lg:max-h-[158px] lg:h-[158px] md:max-h-[80px] md:h-[80px] max-h-32 h-32 overflow-y-scroll cut text-justify">
              <span className="text-sm break-words">{product.description}</span>
            </div>

            {/* quantity and button AddToCart in PC UI start */}
            <div className="lg:grid lg:grid-cols-2 lg:mt-2 mt-3">
              <div className="hidden md:flex md:items-end gap-2">
                <div className="w-fit">
                  <label htmlFor="Quantity" className="sr-only">
                    {" "}
                    Quantity{" "}
                  </label>

                  <div className="flex items-center border border-gray-200 rounded">
                    <button
                      type="button"
                      onClick={decrement}
                      className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                    >
                      &minus;
                    </button>

                    <input
                      type="number"
                      id="Quantity"
                      value={count}
                      className="h-10 w-16 border-transparent text-center focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                      autoComplete="off"
                    />

                    <button
                      onClick={increment}
                      type="button"
                      className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                    >
                      +
                    </button>
                  </div>
                </div>
                <div className="mb-2.5">
                  <span>Stok: {product.stock}</span>
                </div>
              </div>
              <div className="">
                <div className="flex justify-between items-center">
                  <div className="mt-2">
                    <span className="font-semibold text-gray-500">
                      Subtotal :
                    </span>
                  </div>
                  <div>
                    <span className="font-semibold text-xl">
                      {titik(total)}
                    </span>
                  </div>
                </div>
                {localStorage.getItem("role") === "user" ? (
                  <>
                    {count === 0 ? (
                      <button
                        disabled
                        className="bg-[#8bd3c0] w-full mt-2 text-white font-bold py-2 px-4 rounded cursor-not-allowed"
                      >
                        <div className="flex gap-2 items-center justify-center">
                          <div>
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="icon w-5 h-5 fill-current"
                                viewBox="0 0 576 512"
                              >
                                <path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z" />
                              </svg>
                            </span>
                          </div>
                          <div>
                            <span>Keranjang</span>
                          </div>
                        </div>
                      </button>
                    ) : (
                      <button
                        onClick={addToCart}
                        className="bg-[#34bc94] w-full mt-2 cursor-pointer hover:bg-[#13ac84] text-white font-bold py-2 px-4 rounded"
                      >
                        <div className="flex gap-2 items-center justify-center">
                          <div>
                            <span>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="icon w-5 h-5 fill-current"
                                viewBox="0 0 576 512"
                              >
                                <path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z" />
                              </svg>
                            </span>
                          </div>
                          <div>
                            <span>Keranjang</span>
                          </div>
                        </div>
                      </button>
                    )}
                  </>
                ) : (
                  <>
                    {count === 0 ? (
                      <>
                        <button
                          disabled
                          className="bg-[#8bd3c0] w-full mt-2 text-white font-bold py-2 px-4 rounded cursor-not-allowed"
                        >
                          <div className="flex gap-2 items-center justify-center">
                            <div>
                              <span>
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="icon w-5 h-5 fill-current"
                                  viewBox="0 0 576 512"
                                >
                                  <path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z" />
                                </svg>
                              </span>
                            </div>
                            <div>
                              <span>Keranjang</span>
                            </div>
                          </div>
                        </button>
                      </>
                    ) : (
                      <>
                        <button
                          onClick={notYetLogin}
                          className="bg-[#34bc94] w-full cursor-pointer mt-2 hover:bg-[#13ac84] text-white font-bold py-2 px-4 rounded"
                        >
                          <div className="flex gap-2 items-center justify-center">
                            <div>
                              <span>
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  className="icon w-5 h-5 fill-current"
                                  viewBox="0 0 576 512"
                                >
                                  <path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z" />
                                </svg>
                              </span>
                            </div>
                            <div>
                              <span>Keranjang</span>
                            </div>
                          </div>
                        </button>
                      </>
                    )}
                  </>
                )}
              </div>
            </div>
            {/* quantity and button AddToCart in PC UI end */}
          </div>
        </div>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}
