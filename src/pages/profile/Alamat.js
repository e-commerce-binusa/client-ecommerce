import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Swal from "sweetalert2";
import Footer from "../../component/Footer";
import Navbar from "../../component/Navbar";
import SideProfile from "../../component/SideProfile";
import { API_USER } from "../../util/api";
import "../../assets/Profile.css";
import validator from "validator";

export default function Alamat() {
  const [showModalAddress, setShowModalAddress] = useState(false);
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [address, setAddress] = useState("");
  const [err, setErr] = useState(false);

  const getUser = async () => {
    await axios
      .get(`${API_USER}/list/${localStorage.getItem("userId")}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setName(res.data.name);
        setPhoneNumber(res.data.phoneNumber);
        setAddress(res.data.address);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const updateAddress = async (e) => {
    e.preventDefault();
    if (validator.isEmpty(address)) {
      setErr(true);
    } else {
      setErr(false);
      const req = {
        address: address,
      };
      await axios
        .put(`${API_USER}/update/address`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          setShowModalAddress(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil mengubah alamat!",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <div className="bg-gray-100">
      {/* header start */}
      <div>
        <Navbar />
      </div>
      {/* header end */}

      <div className="container mx-auto block rounded md:py-10 py-5 mt-0 md:mt-4 mb-4">
        <div className="grid grid-cols-1 mx-2 md:grid-cols-6 gap-4">
          <div className="hidden md:grid grid-cols-1 border col-span-1 shadow-md p-5 h-[410px] bg-white">
            <SideProfile />
          </div>
          <div className="border col-span-5 shadow-md p-5 h-[410px] bg-white">
            <div className="grid grid-cols-2 justify-between">
              <span className="text-xl font-semibold flex items-center">
                <p>Alamat Saya</p>
              </span>
              <span className="flex justify-end">
                <button
                  className="bg-[#34bc94] text-white md:w-36 w-32 p-2 rounded shadow"
                  onClick={() => setShowModalAddress(true)}
                >
                  Ubah Alamat
                </button>
              </span>
            </div>
            <hr className="my-2" />
            <div className="mt-6">
              <div className="flex space-x-2 md:mb-1 mb-2">
                <h3 className="font-semibold text-lg">{name}</h3>
                <span className="bg-gray-400 w-[0.1px]"></span>
                <p className="text-gray-400 flex items-center">
                  <span>{phoneNumber}</span>
                </p>
              </div>
              <p className="text-gray-400 text-md break-words">{address}</p>
            </div>
          </div>
        </div>
      </div>

      {/* footer start */}
      <div className="footer">
        <Footer />
      </div>
      {/* footer end */}

      {/* modal add Address start */}
      {showModalAddress ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Alamat Baru
                </h2>
                <form onSubmit={updateAddress}>
                  <div className="space-y-4">
                    <div>
                      <label className="sr-only"htmlFor="detail_alamat">
                        Detail Alamat
                      </label>
                      <textarea
                        className="w-full rounded-lg mt-2 border border-[#34bc94] font-semibold p-3 text-sm focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                        placeholder="contoh: *Jl.INDONESIA"
                        rows="4"
                        id="address"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                      ></textarea>
                      {err ? (
                        <p className="text-red-500">field tidak boleh kosong</p>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                  <div className="flex justify-end gap-3 mt-6">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalAddress(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : (
        <></>
      )}
      {/* modal add Address end */}
    </div>
  );
}
