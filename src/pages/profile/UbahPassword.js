import axios from "axios";
import React from "react";
import { useState } from "react";
import Swal from "sweetalert2";
import Navbar from "../../component/Navbar";
import SideProfile from "../../component/SideProfile";
import { API_USER } from "../../util/api";
import "../../assets/Profile.css";
import Footer from "../../component/Footer";
import validator from "validator";

export default function UbahPassword() {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errNew, setErrNew] = useState(false);
  const [errConfirm, setErrConfirm] = useState(false);

  const updatePassword = async (e, status) => {
    e.preventDefault();
    if (validator.isEmpty(password)) {
      setErrNew(true);
      setErrConfirm(false);
    } else if (validator.isEmpty(confirmPassword)) {
      setErrNew(false);
      setErrConfirm(true);
    } else {
      setErrNew(false);
      setErrConfirm(false);
    }
    const req = {
      password: confirmPassword,
    };
    if (confirmPassword !== password) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Password dan confirm password harus sama!",
      });
    } else if (status === 500) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Password harus mengandung kode unik!",
      });
    } else {
      await axios
        .put(`${API_USER}/update/password`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          Swal.fire({
            icon: "success",
            title: "Password berhasil diubah!",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  return (
    <div className="bg-gray-100">
      {/* header start */}
      <Navbar />
      {/* header end */}

      <div className="container mx-auto block rounded md:py-10 py-5 mt-0 md:mt-4 mb-4">
        <div className="grid grid-cols-1 mx-2 md:grid-cols-6 gap-4">
          <div className="hidden md:grid grid-cols-1 border col-span-1 shadow-md p-5 bg-white h-[410px]">
            <SideProfile />
          </div>
          <div className="border col-span-5 shadow-md p-5 bg-white h-[410px]">
            <div>
              <span className="text-xl font-semibold">Atur Password</span>
              <br />
              <span className="text-sm">
                Untuk keamanan akun Anda, mohon untuk tidak menyebarkan password
                Anda ke orang lain.
              </span>
            </div>
            <hr className="my-2" />
            <div className="flex justify-center">
              <form onSubmit={updatePassword}>
                <div className="flex gap-4 items-center my-4">
                  <div className="hidden md:block">
                    <span>Password Yang Baru</span>
                  </div>
                  <div className="md:ml-1">
                    <input
                      type="password"
                      placeholder="Password yang baru"
                      autoComplete="off"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      className="text-sm border border-1 border-gray-300 w-[300px] focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                    />
                    {errNew ? (
                      <p className="text-red-500">field tidak boleh kosong</p>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div className="flex gap-4 items-center my-4">
                  <div className="hidden md:block">
                    <span>Konfirmasi Password</span>
                  </div>
                  <div>
                    <input
                      type="password"
                      placeholder="Konfirmasi password"
                      autoComplete="off"
                      value={confirmPassword}
                      onChange={(e) => setConfirmPassword(e.target.value)}
                      className="text-sm border border-1 border-gray-300 w-[300px] focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                    />
                    {errConfirm ? (
                      <p className="text-red-500">field tidak boleh kosong</p>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
                <div className="flex flex-col justify-end gap-3 mt-6 sm:flex-row">
                  <button className="px-6 py-2 rounded-lg bg-[#34bc94] text-white font-semibold">
                    Konfirmasi
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/* footer start */}
      <div className="bg-gray-100">
        <Footer />
      </div>
      {/* footer end */}
    </div>
  );
}
