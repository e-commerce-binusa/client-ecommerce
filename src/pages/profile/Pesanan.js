import React from "react";
import Navbar from "../../component/Navbar";
import SideProfile from "../../component/SideProfile";
import "../../assets/Pesanan.css";
import { useState } from "react";
import axios from "axios";
import { API_ORDER } from "../../util/api";
import { useEffect } from "react";
import { titik } from "../../util/Rp";
import Footer from "../../component/Footer";
import "../../assets/Profile.css";

export default function Pesanan() {
  const [pesanan, setPesanan] = useState([]);

  const getPesanan = async () => {
    await axios
      .get(`${API_ORDER}/user/list`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setPesanan(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getPesanan();
  }, []);
  return (
    <div className="bg-gray-100">
      {/* header start */}
      <Navbar />
      {/* header end */}

      {/* content start */}
      <div className="container mx-auto block rounded md:mt-4 mt-0 mb-4 md:py-10 py-5">
        <div className="grid grid-cols-1 mx-2 md:grid-cols-6 gap-4">
          <div className="hidden md:grid grid-cols-1 border col-span-1 shadow-md p-5 bg-white h-[410px]">
            <SideProfile />
          </div>
          <div className="border col-span-5 shadow-md pt-6 pb-4 px-6 bg-white md:h-[410px] h-[500px]">
            <div className="text-xl font-semibold mb-4">Riwayat Pesanan</div>
            <hr className="mb-3" />
            <div className="md:h-[300px] h-[400px] overflow-auto scroll-none">
              {pesanan.map((pesanans) => (
                <div className="border p-3 bg-gray-50 my-2" key={pesanans.id}>
                  <div className="grid grid-cols-3 my-3">
                    <div className="col-span-2 flex items-center">
                      <div className="flex gap-x-5">
                        <img
                          src={pesanans.product.image}
                          alt="produk"
                          className="w-20 h-20 border"
                        />
                        <div className="leading-7">
                          <p>{pesanans.product.name}</p>
                          <span>x{pesanans.totalProduct}</span>
                        </div>
                      </div>
                    </div>
                    <div className="flex justify-end items-center">
                      <span>{titik(pesanans.product.price)}</span>
                    </div>
                  </div>
                  <hr />
                  <div className="flex justify-end mt-5 mb-2">
                    <div>
                      <div className="grid grid-cols-2 gap-x-3 mb-5">
                        <span className="text-md flex items-end">
                          Total Pesanan:
                        </span>
                        <h3 className="md:text-xl text-base font-semibold">
                          {titik(pesanans.totalPrice)}
                        </h3>
                      </div>
                      {pesanans.process === true ? (
                        <button
                          disabled
                          className="float-right rounded-md bg-green-200 text-[#13ac84] md:text-base text-sm md:px-5 px-0 py-2 md:w-auto w-40 cursor-not-allowed"
                        >
                          Selesai Diproses
                        </button>
                      ) : (
                        <button
                          disabled
                          className="float-right rounded-md bg-yellow-100 text-yellow-700 md:text-base text-sm md:px-5 px-0 py-2 md:w-auto w-40 cursor-not-allowed"
                        >
                          Sedang Diproses
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      {/* content end */}

      {/* footer start */}
      <div className="footer">
        <Footer />
      </div>
      {/* footer end */}
    </div>
  );
}
