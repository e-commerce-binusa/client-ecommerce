import axios from "axios";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import React, { useState } from "react";
import { useEffect } from "react";
import Swal from "sweetalert2";
import Navbar from "../../component/Navbar";
import SideProfile from "../../component/SideProfile";
import { storage } from "../../firebase";
import { API_USER } from "../../util/api";
import "../../assets/Profile.css";
import Footer from "../../component/Footer";
import validator from "validator";

export default function Profile() {
  const [showModalUsername, setShowModalUsername] = useState(false);
  const [showModalNama, setShowModalNama] = useState(false);
  const [showModalNoTelp, setShowModalNoTelp] = useState(false);
  const [err, setErr] = useState(false);
  const [errNama, setErrName] = useState(false);
  const [errNo, setErrNo] = useState(false);
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [image, setImage] = useState(null);
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  const getUser = async () => {
    await axios
      .get(`${API_USER}/list/${localStorage.getItem("userId")}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUsername(res.data.username);
        setName(res.data.name);
        setImage(res.data.image);
        setEmail(res.data.email);
        setPhoneNumber(res.data.phoneNumber);
        // console.log(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const updateUsername = async (e) => {
    e.preventDefault();
    if (validator.isEmpty(username)) {
      setErr(true);
    } else {
      setErr(false);
      const req = {
        username: username,
      };
      await axios
        .put(`${API_USER}/update/username`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          setShowModalUsername(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil mengubah username!",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const updateName = async (e) => {
    e.preventDefault();
    if (validator.isEmpty(name)) {
      setErrName(true);
    } else {
      setErrName(false);
      const req = {
        name: name,
      };
      await axios
        .put(`${API_USER}/update/name`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          setShowModalNama(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil mengubah nama!",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const updatePhoneNumber = async (e) => {
    e.preventDefault();
    if (validator.isEmpty(phoneNumber)) {
      setErrNo(true);
    } else {
      setErrNo(false);
      const req = {
        phoneNumber: phoneNumber,
      };
      await axios
        .put(`${API_USER}/update/phoneNumber`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          setShowModalNoTelp(false);
          Swal.fire({
            icon: "success",
            title: "Berhasil mengubah nomor telepon!",
            showConfirmButton: false,
            timer: 1500,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const updateHandler = async (url) => {
    const req = {
      image: url,
    };
    await Swal.fire({
      title: "Ingin menyimpan perubahan?",
      icon: "question",
      showDenyButton: true,
      confirmButtonText: "Ya, simpan!",
      denyButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put(`${API_USER}/update/image`, req, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        Swal.fire({
          icon: "success",
          title: "Berhasil mengubah avatar!",
          showConfirmButton: false,
          timer: 1500,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "info",
          title: "Perubahan avatar Anda tidak tersimpan!",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };

  const submitUpdate = () => {
    const storageRef = ref(storage, `${image.name}`);

    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          updateHandler(downloadUrl);
        });
      });
  };

  const saveUpdate = (e) => {
    e.preventDefault();
    submitUpdate();
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <div className="bg-gray-100">
      {/* header start */}
      <div>
        <Navbar />
      </div>
      {/* header end */}

      <div className="container mx-auto block rounded md:py-10 py-5 md:mt-4 mt-0 mb-4">
        <div className="grid grid-cols-1 mx-2 md:grid-cols-6 gap-4">
          <div className="hidden md:grid grid-cols-1 border col-span-1 shadow-md p-5 h-[410px] bg-white">
            <SideProfile />
          </div>
          <div className="border col-span-5 shadow-md p-5 lg:h-[410px] md:h-[410px] h-[600px] bg-white">
            <div>
              <span className="text-xl font-semibold">Profil Saya</span>
              <br />
              <span className="text-sm hidden md:block">
                Kelola informasi profil Anda untuk mengontrol, melindungi dan
                mengamankan akun.
              </span>
            </div>
            <hr className="md:my-2 my-3" />
            <div className="md:grid md:grid-cols-5 md:justify-between gap-6">
              {/* PC UI start */}
              <div className="hidden md:block py-10 border border-t-0 border-l-0 border-b-0 border-r-1 col-span-3">
                <div className="grid grid-cols-4 space-x-10">
                  <div className="col-span-1 text-gray-500 text-md">
                    <span>Username</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3 text-md space-x-2">
                    <span>{username}</span>
                    <span
                      onClick={() => {
                        setShowModalUsername(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
                <div className="grid grid-cols-4 space-x-10 mt-4">
                  <div className="col-span-1 text-gray-500 text-md">
                    <span>Nama</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3 text-md space-x-2">
                    <span>{name}</span>
                    <span
                      onClick={() => {
                        setShowModalNama(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
                <div className="grid grid-cols-4 space-x-10 mt-4">
                  <div className="col-span-1 text-gray-500 text-md">
                    <span>Email</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3 text-md space-x-2">
                    <span>{email}</span>
                  </div>
                </div>
                <div className="grid grid-cols-4 space-x-10 mt-4">
                  <div className="col-span-1 text-gray-500 text-md">
                    <span>No Telepon</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3 text-md space-x-2">
                    <span>{phoneNumber}</span>
                    <span
                      onClick={() => {
                        setShowModalNoTelp(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
              </div>
              {/* PC UI end */}

              <div className="col-span-2 my-2">
                <div className="flex justify-center">
                  <img
                    className="w-[150px] h-[150px] rounded-full"
                    src={image}
                    alt="profile"
                  />
                </div>
                <form className="space-y-4" onSubmit={saveUpdate}>
                  <div className="my-4">
                    <label
                      className="block mb-2 text-sm font-medium text-gray-900"
                     htmlFor="file_input"
                    >
                      Upload file
                    </label>
                    <input
                      className="block w-full text-sm focus:ring-teal-500 focus:border-green-400 text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 focus:outline-none"
                      aria-describedby="file_input_help"
                      id="file_input"
                      type="file"
                      autoComplete="off"
                      required
                      onChange={(e) => setImage(e.target.files[0])}
                    />
                  </div>
                  <div className="flex justify-end gap-3 mt-6">
                    <button
                      type="submit"
                      className="px-6 py-2 md:w-[100px] w-full rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Save
                    </button>
                  </div>
                </form>
              </div>

              {/* Mobile UI start */}
              <div className="block md:hidden mt-10 p-3">
                <div className="grid grid-cols-4 space-x-14">
                  <div className="col-span-1 text-gray-500 w-28">
                    <span>Username</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3">
                    {username}{" "}
                    <span
                      onClick={() => {
                        setShowModalUsername(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
                <div className="grid grid-cols-4 mt-4 space-x-14">
                  <div className="col-span-1 text-gray-500 w-28">
                    <span>Nama</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3">
                    {name}{" "}
                    <span
                      onClick={() => {
                        setShowModalNama(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
                <div className="grid grid-cols-4 mt-4 space-x-14">
                  <div className="col-span-1 text-gray-500 w-28">
                    <span>Email</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3">{email} </div>
                </div>
                <div className="grid grid-cols-4 mt-4 space-x-14">
                  <div className="col-span-1 text-gray-500 w-28">
                    <span>No Telepon</span>
                    <span className="float-right">:</span>
                  </div>
                  <div className="col-span-3">
                    {phoneNumber}{" "}
                    <span
                      onClick={() => {
                        setShowModalNoTelp(true);
                      }}
                      className="text-blue-600 underline underline-offset-1 cursor-pointer"
                    >
                      ubah
                    </span>
                  </div>
                </div>
              </div>
              {/* Mobile UI end */}
            </div>
          </div>
        </div>

        {/* MODAL USERNAME */}
        {showModalUsername ? (
          <>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                  <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                    Edit Username
                  </h2>
                  <form className="space-y-4" onSubmit={updateUsername}>
                    <div>
                      <label className="sr-only"htmlFor="name">
                        Username
                      </label>
                      <input
                        className="w-full rounded-lg border border-[#34bc94] font-semibold p-3 text-sm focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                        placeholder="Username"
                        type="text"
                        id="username"
                        autoComplete="off"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                      {err ? (
                        <p className="text-red-500">field tidak boleh kosong</p>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="flex justify-between gap-3 mt-6">
                      <button
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                        onClick={() => setShowModalUsername(false)}
                      >
                        Tutup
                      </button>
                      <button
                        type="submit"
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}

        {/* MODAL NAMA */}
        {showModalNama ? (
          <>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                  <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                    Edit Nama
                  </h2>
                  <form onSubmit={updateName} className="space-y-4">
                    <div>
                      <label className="sr-only"htmlFor="name">
                        Nama
                      </label>
                      <input
                        className="w-full rounded-lg border border-[#34bc94] font-semibold p-3 text-sm focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                        placeholder="Nama"
                        type="text"
                        id="nama"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        autoComplete="off"
                      />
                      {errNama ? (
                        <p className="text-red-500">field tidak boleh kosong</p>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="flex justify-between gap-3 mt-6">
                      <button
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                        onClick={() => setShowModalNama(false)}
                      >
                        Tutup
                      </button>
                      <button
                        type="submit"
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}

        {/* MODAL NO TELEPON */}
        {showModalNoTelp ? (
          <>
            <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
              <div className="relative w-auto my-6 mx-auto max-w-3xl">
                <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                  <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                    Edit No Telepon
                  </h2>
                  <form onSubmit={updatePhoneNumber} className="space-y-4">
                    <div>
                      <label className="sr-only"htmlFor="name">
                        No Telepon
                      </label>
                      <input
                        className="w-full rounded-lg border border-[#34bc94] font-semibold p-3 text-sm focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                        placeholder="No Telepon"
                        type="text"
                        id="noTelp"
                        value={phoneNumber}
                        onChange={(e) => setPhoneNumber(e.target.value)}
                        autoComplete="off"
                      />
                      {errNo ? (
                        <p className="text-red-500">field tidak boleh kosong</p>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="flex justify-between gap-3 mt-6">
                      <button
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                        onClick={() => setShowModalNoTelp(false)}
                      >
                        Tutup
                      </button>
                      <button
                        type="submit"
                        className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
          </>
        ) : null}
      </div>

      {/* footer start */}
      <div className="footer">
        <Footer />
      </div>
      {/* footer end */}
    </div>
  );
}
