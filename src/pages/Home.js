import axios from "axios";
import { Carousel } from "flowbite-react";
import React, { useEffect, useState } from "react";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import { API_CATEGORY, API_PRODUCT } from "../util/api";
import { titik } from "../util/Rp";
import carousel1 from "../assets/image/carousel-1.png";
import carousel2 from "../assets/image/carousel-2.jpg";
import carousel3 from "../assets/image/carousel-3.jpg";

export default function Home() {
  const [product, setProduct] = useState([]);
  const [category, setCategory] = useState([]);

  const getAllProduct = async () => {
    await axios
      .get(`${API_PRODUCT}/all`)
      .then((res) => {
        setProduct(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllCategory = async () => {
    await axios
      .get(`${API_CATEGORY}/all`)
      .then((res) => {
        setCategory(res.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getAllProduct();
    getAllCategory();
  }, []);
  return (
    <div className="flex justify-center overflow-x-hidden">
      <div className="bg-gray-100 max-w-screen-2xl">
        {/* header start */}
        <div>
          <Navbar />
        </div>
        {/* header end */}

        {/* content start */}
        <div className="mb-5">
          <div className="grid grid-cols-1 md:grid-cols-3 py-5 px-5 md:px-20 gap-x-2 shadow bg-white">
            {/* Carousel start */}
            <div className="col-span-2 h-56 sm:h-64 xl:h-80 2xl:h-96">
              <Carousel>
                <img src={carousel1} alt="..." />
                <img src={carousel2} alt="..." />
                <img src={carousel3} alt="..." />
              </Carousel>
            </div>
            {/* Carousel end */}
            <div className="hidden md:grid md:grid-rows-1 md:gap-y-1">
              <img
                src="https://thumbs.dreamstime.com/b/flyer-invitation-plus-size-shop-big-sale-lettering-overweight-women-choose-clothes-hangers-store-department-womens-clothing-153063757.jpg"
                alt=""
                className="lg:h-[155px] md:h-32 w-full rounded-md border"
              />
              <img
                src="https://img.freepik.com/premium-vector/online-shopping-huge-smartphone-with-order-now-text-screen-women-choose-clothes-buying-products-internet-purchase-mobile-fashion-boutique-sale-banner-vector-cartoon-flat-style-isolated-concept_176411-3894.jpg"
                alt=""
                className="lg:h-[155px] md:h-32 w-full rounded-md border"
              />
            </div>
          </div>

          {/* Category start */}
          <div className="my-5">
            <section className="flex justify-center">
              <div className="max-w-screen-xl py-5 rounded-md bg-white">
                <header className="flex justify-center mb-5">
                  <h3 className="md:text-xl text-lg font-medium uppercase border-b-2 border-b-black w-auto">
                    Kategori
                  </h3>
                </header>
                <div className="grid grid-cols-3 md:grid md:grid-cols-6 lg:flex lg:flex-wrap lg:gap-2 md:gap-2 gap-3 mx-5 lg:mx-20 md:mx-10">
                  {category.map((categories) => (
                    <a
                      href={"/category/" + categories.id}
                      key={categories.id}
                      className="border border-gray-300 rounded-md text-center flex justify-center py-2 w-auto px-2 md:py-2 md:px-2 lg:py-2.5 lg:px-5 bg-[#13ac84] text-white hover:bg-[#5cc4a4] hover:text-teal-900 transition duration-500 hover:transition hover:duration-500 hover:shadow-md cursor-pointer"
                    >
                      <p>{categories.name}</p>
                    </a>
                  ))}
                </div>
              </div>
            </section>
          </div>
          {/* Category end */}

          {/* Product start */}
          <section className="flex justify-center">
            <div className="max-w-screen-xl py-5 mx-auto md:mx-5 lg:mx-20 w-full rounded-md">
              <header className="flex justify-center mb-1 md:mb-3 bg-white p-3 shadow">
                <h3 className="md:text-xl text-lg font-bold uppercase border-b-2 border-b-black w-auto">
                  Product
                </h3>
              </header>
              <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-6 gap-y-2 md:mx-0 mx-1">
                {product.map((products) => (
                  <a href={"/product/" + products.id} key={products.id}>
                    <div className="relative max-w-sm bg-white border border-gray-200 rounded-sm h-72 mx-[3px] transition duration-300 hover:shadow-md hover:transition hover:duration-300 hover:scale-105 md:hover:scale-[1.06]">
                      <img
                        className="rounded-t-sm h-44 w-full"
                        src={products.image}
                        alt="product"
                      />
                      <div className="p-3 md:all lg:break-words">
                        <p className="font-semibold">{products.name}</p>
                        <div className="absolute bottom-2">
                          <div>
                            <p className="text-gray-700 text-sm text-right">
                              {titik(products.price)}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                ))}
              </div>
            </div>
          </section>
          {/* Product end */}

          {/* Pagination start */}
          {/* <section>
            <ol className="flex justify-center gap-1 text-xs font-medium">
              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  <span className="sr-only">Prev Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                      clipRule="evenodd"
                    />
                  </svg>
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded bg-[#13ac84] text-white text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  1
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  2
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  3
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  4
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  <span className="sr-only">Next Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clipRule="evenodd"
                    />
                  </svg>
                </a>
              </li>
            </ol>
          </section> */}
          {/* Pagination end */}
        </div>
        {/* content end */}

        {/* footer start */}
        <div>
          <Footer />
        </div>
        {/* footer end */}
      </div>
    </div>
  );
}
