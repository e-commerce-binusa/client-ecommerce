import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useParams } from "react-router-dom";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import { API_CATEGORY } from "../util/api";
import { titik } from "../util/Rp";
import image from "../assets/image/carousel-3.jpg"

export default function CategoryById() {
  const [categoryId, setCategoryId] = useState({
    name: "",
    product: [],
  });
  const { id } = useParams();

  const getCategoryId = async () => {
    await axios
      .get(`${API_CATEGORY}/id/${id}`)
      .then((res) => {
        setCategoryId(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getCategoryId();
  }, []);

  return (
    <div className="flex justify-center overflow-x-hidden">
      <div className="bg-gray-100 max-w-screen-2xl">
        {/* header start */}
        <div>
          <Navbar />
        </div>
        {/* header end */}

        {/* content start */}
        <div className="lg:my-10 md:mt-5 md:mb-10 mb-5">
          <div className="flex justify-center mb-1.5 md:mb-3 lg:mb-10">
            <img
              src={image}
              alt="poster-diskon"
              className="w-full h-40 md:w-4/5 md:h-52 lg:w-4/5 lg:h-96"
            />
          </div>
          {/* Product start */}
          <section className="">
            <div className="max-w-screen-xl py-5 mx-auto md:mx-5 lg:mx-20 rounded-md">
              <header className="flex justify-center mb-1 md:mb-3 bg-white p-3 shadow">
                <h3 className="md:text-xl text-lg font-bold uppercase border-b-2 border-b-black w-auto">
                  Kategori {categoryId.name}
                </h3>
              </header>
              <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-6 gap-y-2 md:mx-0 mx-1">
                {categoryId.product.map((products) => (
                <a href={"/product/" + products.id} key={products.id}>
                  <div className="relative max-w-sm bg-white border border-gray-200 rounded-sm h-72 mx-[3px] transition duration-300 hover:shadow-md hover:transition hover:duration-300 hover:scale-105 md:hover:scale-[1.06]">
                    <img
                      className="rounded-t-sm h-44 w-full"
                      src={products.image}
                      alt=""
                    />
                    <div className="p-3">
                      <p className="font-semibold">{products.name}</p>
                      <div className=" absolute bottom-2 left-3">
                        <p className="text-gray-700 text-sm">{titik(products.price)}</p>
                      </div>
                    </div>
                  </div>
                </a>
                ))}
              </div>
            </div>
          </section>
          {/* Product end */}

          {/* Pagination start */}
          {/* <section>
            <ol className="flex justify-center gap-1 text-xs font-medium">
              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  <span className="sr-only">Prev Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                      clipRule="evenodd"
                    />
                  </svg>
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded bg-[#13ac84] text-white text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  1
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  2
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  3
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  4
                </a>
              </li>

              <li className="border border-gray-300 rounded-md">
                <a
                  href="#"
                  className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                >
                  <span className="sr-only">Next Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clipRule="evenodd"
                    />
                  </svg>
                </a>
              </li>
            </ol>
          </section> */}
          {/* Pagination end */}
        </div>
        {/* content end */}

        {/* footer start */}
        <div>
          <Footer />
        </div>
        {/* footer end */}
      </div>
    </div>
  );
}
