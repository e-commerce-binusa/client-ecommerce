import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import { API_PRODUCT } from "../util/api";
import { titik } from "../util/Rp";
import notFound from "../assets/image/not-found.png";

export default function SearchProduct() {
  const [product, setProduct] = useState([]);
  // console.log(window.location.search);

  const getProduct = async () => {
    await axios
      .get(`${API_PRODUCT}/search${window.location.search}`)
      .then((res) => {
        setProduct(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  console.log(product);

  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div className="flex justify-center overflow-x-hidden">
      <div className="bg-gray-100 max-w-screen-2xl">
        {/* header start */}
        <div>
          <Navbar />
        </div>
        {/* header end */}

        {/* content start */}
        <div className="md:mt-5 mb-16">
          {product.length === 0 ? (
            <div className="flex justify-center lg:my-28 my-32">
              <div>
                <img src={notFound} alt="not-found" className="md:w-96 md:h-72 w-72 h-52" />
                <p className="text-xl text-center text-gray-500 font-semibold">
                  Tidak Ditemukan
                </p>
                <p className="text-md text-center text-gray-400 font-medium">
                  Mohon coba kata kunci lain
                </p>
              </div>
            </div>
          ) : (
            <div className="">
              <header className="flex justify-center mb-1 md:mb-3 bg-white p-3 md:mx-20 mx-1 my-0 shadow">
                <h3 className="md:text-xl text-lg font-bold uppercase border-b-2 border-b-black w-auto">
                  Product Terkait
                </h3>
              </header>
              {/* Product start */}
              <section className="">
                <div className="max-w-screen-xl py-0 mx-auto md:mx-5 lg:mx-20 rounded-md">
                  <div className="grid grid-cols-2 md:grid-cols-4 lg:grid-cols-6 gap-y-2 md:mx-0 mx-1">
                    {product.map((products) => (
                      <a href={"/product/" + products.id}>
                        <div className="relative max-w-sm bg-white border border-gray-200 rounded-sm h-60 mx-[3px] transition duration-300 hover:shadow-md hover:transition hover:duration-300 hover:scale-105 md:hover:scale-[1.06]">
                          <img
                            className="rounded-t-sm h-32 w-full"
                            src={products.image}
                            alt=""
                          />
                          <div className="p-3">
                            <p className="font-semibold">{products.name}</p>
                            <div className=" absolute bottom-2 left-3">
                              <p className="text-gray-700 text-sm">
                                {titik(products.price)}
                              </p>
                            </div>
                          </div>
                        </div>
                      </a>
                    ))}
                  </div>
                </div>
              </section>
              {/* Product end */}

              {/* Pagination start */}
              {/* <section>
                <ol className="flex justify-center gap-1 text-xs font-medium">
                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      <span className="sr-only">Prev Page</span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-3 w-3"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </a>
                  </li>

                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="block h-8 w-8 rounded bg-[#13ac84] text-white text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      1
                    </a>
                  </li>

                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      2
                    </a>
                  </li>

                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      3
                    </a>
                  </li>

                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="block h-8 w-8 rounded  text-center leading-8 transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      4
                    </a>
                  </li>

                  <li className="border border-gray-300 rounded-md">
                    <a
                      href="#"
                      className="inline-flex h-8 w-8 items-center justify-center rounded transition duration-500 hover:transition hover:duration-500 hover:bg-[#74ccb6] hover:text-white"
                    >
                      <span className="sr-only">Next Page</span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-3 w-3"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </a>
                  </li>
                </ol>
              </section> */}
              {/* Pagination end */}
            </div>
          )}
        </div>
        {/* content end */}

        {/* footer start */}
        <div>
          <Footer />
        </div>
        {/* footer end */}
      </div>
    </div>
  );
}
