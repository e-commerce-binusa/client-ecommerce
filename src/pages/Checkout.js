import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../assets/Checkout.css";
import Footer from "../component/Footer";
import Navbar from "../component/Navbar";
import { API_CART, API_ORDER, API_USER } from "../util/api";
import { titik } from "../util/Rp";

function Checkout() {
  const [showModalAddress, setShowModalAddress] = useState(false);
  const [message, setMessage] = useState("");
  const [user, setUser] = useState({
    name: "",
    phoneNumber: "",
  });
  const [address, setAddress] = useState("");
  const [cart, setCart] = useState({
    cartItems: [],
    totalCost: 0,
    quantity: 0,
  });
  const navigate = useNavigate();

  const getAllCart = async () => {
    await axios
      .get(`${API_CART}/list`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        if (res.status == 200) {
          setCart(res.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getUser = async () => {
    await axios
      .get(`${API_USER}/list/${localStorage.getItem("userId")}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setUser(res.data);
        setAddress(res.data.address);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const updateAddress = async (e) => {
    e.preventDefault();
    const req = {
      address: address,
    };
    await axios
      .put(`${API_USER}/update/address`, req, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then(() => {
        setShowModalAddress(false);
        Swal.fire({
          icon: 'success',
          title: 'Berhasil mengubah alamat!',
          showConfirmButton: false,
          timer: 1500
        })
        getUser();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const transaction = async (e) => {
    e.preventDefault();
    await Swal.fire({
      title: "Yakin ingin memesan produk ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Pesan!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        const req = cart.cartItems.map((carts) => ({
          product: {
            id: carts.product.id,
          },
          totalPrice: carts.product.price * carts.quantity,
          message: message,
          totalProduct: carts.quantity,
        }));
        axios
          .post(`${API_ORDER}/add`, req, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            axios
              .delete(`${API_CART}/delete`, {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              })
              .then(() => {
                Swal.fire({
                  icon: 'success',
                  title: 'Berhasil Memesan!',
                  showConfirmButton: false,
                  timer: 1500
                })
                navigate("/sukses");
              })
              .catch((error) => {
                console.log(error);
              });
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };

  useEffect(() => {
    getAllCart();
    getUser();
  }, []);

  return (
    <div>
      {/* header start */}
      <div>
        <Navbar />{" "}
      </div>

      {/* header end */}

      <div className="container grid grid-cols-1 md:grid-cols-3 gap-6 mx-auto rounded p-8 mt-4 mb-4">
        {/* ALAMAT */}
        <div className="border w-[19.3rem] md:w-full col-span-1 shadow-md p-5">
          <div className="text-xl">Produk Dipesan</div>
          <hr className="mt-4" />
          <div className="cut checkout">
            {cart.cartItems.map((carts) => (
              <div className="block mt-4" key={carts.id}>
                <div className="col-span-3 flex items-center md:gap-5 gap-x-3">
                  <div className="">
                    <img
                      className="md:w-[60px] md:h-[70px] w-20 h-20"
                      src={carts.product.image}
                      alt="produk"
                    />
                  </div>
                  <div className="w-40">
                    <div className="cut-text">
                      <span className="text-md font-medium">{carts.product.name}</span>
                    </div>
                    <div>
                      <span className="text-sm text-gray-700">{carts.product.description}</span>
                    </div>
                    <div className="flex justify-between">
                    <div className="">
                      <span className="text-gray-400 text-sm mt-4">
                        x{carts.quantity}
                      </span>
                    </div>
                    <div className="">
                      <span className="text-gray-400 text-sm mt-4">
                        {titik(carts.product.price)}
                      </span>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <hr className="my-4" />
          <div className="flex items-center justify-between">
            <div>
              <span className="font-semibold">Jumlah produk</span>
            </div>
            <div>
              <span className="font-semibold text-gray-500">
                ( {cart.quantity} produk )
              </span>
            </div>
          </div>
          <br />
          <div className="flex items-center justify-between">
            <div>
              <span className="font-semibold">Total harga</span>
            </div>
            <div>
              <span className="text-xl font-semibold">
                {titik(cart.totalCost)}
              </span>
            </div>
          </div>
        </div>
        <div className="col-span-2">
          <div className="border shadow-md p-5">
            <div className="flex gap-2 text-xl">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-[20px] h-[20px]"
                viewBox="0 0 384 512"
              >
                <path d="M215.7 499.2C267 435 384 279.4 384 192C384 86 298 0 192 0S0 86 0 192c0 87.4 117 243 168.3 307.2c12.3 15.3 35.1 15.3 47.4 0zM192 256c-35.3 0-64-28.7-64-64s28.7-64 64-64s64 28.7 64 64s-28.7 64-64 64z" />
              </svg>
              Alamat Pengiriman
            </div>
            <div className="font-bold">
              <div className="flex my-2 space-x-2">
                <h3 className="font-semibold text-lg">{user.name}</h3>
                <span className="bg-gray-400 w-[0.1px]"></span>
                <p className="text-gray-400">{user.phoneNumber}</p>
              </div>
              <span className=" font-normal">{user.address}</span>
              <span
                onClick={() => setShowModalAddress(true)}
                className="text-blue-500 font-normal float-right cursor-pointer"
              >
                Ubah
              </span>
            </div>
            <hr className="my-4" />
            <form onSubmit={transaction}>
              <div className=" mb-4 md:mb-6">
                <label htmlFor="catatan">
                  Catatan: <span className="text-red-600 font-medium">*</span>
                </label>
                <textarea
                  className="w-full rounded-md font-semibold p-3 mt-2 text-sm border-gray-400 focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                  placeholder="contoh: *ukuran xl"
                  rows="9"
                  id="description"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                ></textarea>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="w-full md:w-auto inline-block px-4 py-2 bg-[#3ca588] text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-[#4ac4a5] hover:shadow-lg transition duration-150 ease-in-out"
                >
                  Checkout
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>

      {/* footer start */}
      <div className="footer">
        <Footer/>
      </div>
      {/* footer end */}

      {/* modal add Address start */}
      {showModalAddress ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="flex flex-col max-w-md gap-2 p-6 rounded-md shadow-md bg-white text-[#34bc94]">
                <h2 className="flex items-center gap-2 text-xl mb-2 font-semibold leading-tight tracking-wide">
                  Ubah Alamat
                </h2>
                <form onSubmit={updateAddress}>
                  <div className="space-y-4">
                    <div>
                      <label className="sr-only"htmlFor="detail_alamat">
                        Detail Alamat
                      </label>
                      <textarea
                        className="w-full rounded-lg mt-2 border border-[#34bc94] font-semibold p-3 text-sm focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                        placeholder="contoh: *Jl.INDONESIA"
                        rows="4"
                        id="address"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                      ></textarea>
                    </div>
                  </div>
                  <div className="flex justify-end gap-3 mt-6">
                    <button
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold hover:bg-red-500"
                      onClick={() => setShowModalAddress(false)}
                    >
                      Tutup
                    </button>
                    <button
                      type="submit"
                      className="px-6 py-2 w-[100px] rounded-lg bg-[#34bc94] text-white font-semibold"
                    >
                      Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : (
        <></>
      )}
      {/* modal add Address end */}
    </div>
  );
}

export default Checkout;
