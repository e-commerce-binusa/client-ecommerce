import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import image from "../assets/image/logoo.png";
import validator from "validator";
import { API_USER } from "../util/api";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const [error, setError] = useState(false);
  const [err, setErr] = useState(false);

  const navigate = useNavigate();

  const login = async (e) => {
    e.preventDefault();

    if (validator.isEmpty(email)) {
      setError(true);
      setErr(false);
    } else if (validator.isEmpty(password)) {
      setError(false);
      setErr(true);
    } else {
      setError(false);
      setErr(false);
    }

    try {
      const { data, status } = await axios.post(API_USER + "/login", {
        email: email,
        password: password,
      });
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Sukses!",
          text: "Login telah berhasil!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.userId);
        localStorage.setItem("token", data.jwttoken);
        localStorage.setItem("role", data.role);
        if (localStorage.getItem("role") === "admin") {
          navigate("/admin/dashboard");
        } else {
          navigate("/");
        }
      }
    } catch (error) {
      Swal.fire({
        position: "center",
        icon: "warning",
        title: "Email atau Password yang Anda masukan salah.",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };

  const togglePassword = () => {
    if (passwordType === "password") {
      setPasswordType("text");
      return;
    }
    setPasswordType("password");
  };
  return (
    <div className="flex items-center justify-center bg-white h-screen">
      <div className="text-gray-800">
        <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-2 gap-x-20">
          <div className="flex items-center justify-center">
            <img
              src={image}
              alt="BINUSA"
              className="w-64 h-56 md:w-[26rem] md:h-[22rem]"
            />
          </div>
          <div className="border rounded-md shadow-lg shadow-[#3ca588] p-7">
            <form onSubmit={login} className="">
              <div className="flex flex-row items-center justify-center lg:justify-start mb-6">
                <p className="text-xl font-semibold mb-0">Log in</p>
              </div>

              <div className="mb-3">
                <label className="font-medium" htmlFor="email">
                  Email
                </label>
                <input
                  type="text"
                  className="form-control block w-full px-4 py-2 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-white focus:ring-teal-500 focus:outline-none"
                  id="exampleFormControlInput2"
                  placeholder="Email"
                  autoComplete="off"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {error ? (
                  <p className="text-red-500">field tidak boleh kosong</p>
                ) : (
                  <></>
                )}
              </div>

              <div className="mb-3">
                <div className="relative  mt-1 text-black">
                  <label className="font-medium" htmlFor="password">
                    Password
                  </label>
                  <input
                    autoComplete="off"
                    type={passwordType}
                    className="form-control block w-full px-4 py-2 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-white focus:ring-teal-500 focus:outline-none"
                    placeholder="Kata Sandi"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />

                  <span
                    onClick={togglePassword}
                    className="absolute inset-y-0 top-6 right-4 inline-flex items-center cursor-pointer"
                  >
                    {passwordType === "password" ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-eye"
                        viewBox="0 0 16 16"
                      >
                        <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                        <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-eye-slash"
                        viewBox="0 0 16 16"
                      >
                        <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z" />
                        <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z" />
                        <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709zm10.296 8.884-12-12 .708-.708 12 12-.708.708z" />
                      </svg>
                    )}
                  </span>
                </div>
                  
                {err ? (
                    <p className="text-red-500">field tidak boleh kosong</p>
                  ) : (
                    <></>
                  )}
              </div>

              <div className="text-center mt-7">
                <button
                  type="submit"
                  className="inline-block w-full px-7 py-3 bg-[#3ca588] text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-[#4ac4a5] hover:shadow-lg transition duration-150 ease-in-out"
                >
                  Login
                </button>
                <p className="text-sm font-semibold mt-3 pt-1 mb-0">
                  Belum memiliki akun?
                  <a
                    href="/signup"
                    className="text-red-600 ml-1 hover:text-red-700 focus:text-red-700 transition duration-200 ease-in-out"
                  >
                    Register
                  </a>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
