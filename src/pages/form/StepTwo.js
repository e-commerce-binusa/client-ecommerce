import React, { useState } from "react";
import validator from "validator";
import image from "../../assets/image/logoo.png";

const StepTwo = ({
  nextStep,
  prevStep,
  name,
  phoneNumber,
  address,
  setName,
  setPhoneNumber,
  setAddress,
}) => {
  //creating error state for validation
  const [error, setError] = useState(false);
  const [errorTelp, setErrorTelp] = useState(false);
  const [errorAlamat, setErrorAlamat] = useState(false);

  const submitFormData = (e) => {
    e.preventDefault();

    if (validator.isEmpty(name)) {
      setError(true);
      setErrorTelp(false);
      setErrorAlamat(false);
    } else if (validator.isEmpty(phoneNumber)) {
      setError(false);
      setErrorTelp(true);
      setErrorAlamat(false);
    } else if (validator.isEmpty(address)) {
      setError(false);
      setErrorTelp(false);
      setErrorAlamat(true);
    } else {
      nextStep();
    }
  };
  return (
    <div className="flex items-center justify-center bg-white h-screen">
      <div className="text-gray-800">
        <div className="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-2 gap-x-20">
          <div className="flex items-center justify-center">
            <img
              src={image}
              alt="BINUSA"
              className="w-64 h-56 md:w-[26rem] md:h-[22rem]"
            />
          </div>
          <div className="border rounded-md shadow-lg shadow-[#3ca588] p-7">
            <form onSubmit={submitFormData}>
              <div className="flex flex-row items-center justify-center lg:justify-start mb-6">
                <p className="text-xl font-semibold mb-0">Daftar</p>
              </div>

              <div className="mb-3">
                <label className="font-medium" htmlFor="name">
                  Nama Lengkap
                </label>
                <input
                  type="text"
                  className="form-control block w-full px-4 py-2 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                  id="name"
                  placeholder="Nama Lengkap"
                  defaultValue={name}
                  onChange={(e) => setName(e.target.value)}
                  autoComplete="off"
                />
                {error ? (
                  <p className="text-red-500">field tidak boleh kosong</p>
                ) : (
                  ""
                )}
              </div>

              <div className="mb-3">
                <label className="font-medium" htmlFor="phoneNumber">
                  Nomor Telepon
                </label>
                <input
                  type="number"
                  className="form-control block w-full px-4 py-2.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84] [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                  id="phoneNumber"
                  placeholder="No. Telp"
                  defaultValue={phoneNumber}
                  onChange={(e) => setPhoneNumber(e.target.value)}
                  autoComplete="off"
                />
                {errorTelp ? (
                  <p className="text-red-500">field tidak boleh kosong</p>
                ) : (
                  ""
                )}
              </div>

              <div className="mb-3">
                <label className="font-medium" htmlFor="address">
                  Alamat
                </label>
                <input
                  type="text"
                  className="form-control block w-full px-4 py-2 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:ring-[#13ac84] focus:outline-none focus:border-[#13ac84]"
                  id="address"
                  placeholder="Alamat"
                  defaultValue={address}
                  onChange={(e) => setAddress(e.target.value)}
                  autoComplete="off"
                />
                {errorAlamat ? (
                  <p className="text-red-500">field tidak boleh kosong</p>
                ) : (
                  ""
                )}
              </div>

              <div className="text-center flex justify-between lg:text-left">
                <button
                  onClick={prevStep}
                  type="submit"
                  className="inline-block lg:w-[150px] md:w-[150px] w-20 lg:px-7 md:px-7 px-2 py-3 bg-[#3ca588] text-white font-medium lg:text-sm md:text-base text-xs leading-snug uppercase rounded shadow-md hover:bg-[#4ac4a5] hover:shadow-lg transition duration-150 ease-in-out"
                >
                  Previous
                </button>
                <button
                  type="submit"
                  className="inline-block lg:w-[150px] md:w-[150px] w-20 lg:px-7 md:px-7 px-2 py-3 bg-[#3ca588] text-white font-medium lg:text-sm md:text-base text-xs leading-snug uppercase rounded shadow-md hover:bg-[#4ac4a5] hover:shadow-lg transition duration-150 ease-in-out"
                >
                  Next
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StepTwo;
