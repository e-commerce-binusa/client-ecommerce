import React, { useState } from "react";
import "../assets/Cart.css";
import Footer from "../component/Footer";
import { useEffect } from "react";
import axios from "axios";
import { titik } from "../util/Rp";
import { API_CART } from "../util/api";
import Navbar from "../component/Navbar";
import Swal from "sweetalert2";
import empty from "../assets/image/no-data.png";

export default function Cart() {
  const [cart, setCart] = useState({
    cartItems: [],
    totalCost: 0,
    quantity: 0,
  });

  const getAllCart = async () => {
    await axios
      .get(`${API_CART}/list`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        if (res.status === 200) {
          setCart(res.data);
        }
      });
  };

  const decrement = async (carts) => {
    if (cart.quantity === 1) {
      await axios
        .get(`${API_CART}/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          const req = {
            quantity: 1,
          };
          axios
            .put(`${API_CART}/update/${carts.id}`, req, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then(() => {
              getAllCart();
            })
            .catch((err) => {
              console.log(err);
            });
        });
    } else {
      await axios
        .get(`${API_CART}/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          const req = {
            quantity: carts.quantity - 1,
          };
          axios
            .put(`${API_CART}/update/${carts.id}`, req, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then(() => {
              getAllCart();
            })
            .catch((err) => {
              console.log(err);
            });
        });
    }
  };

  const increment = async (carts) => {
    if (cart.quantity === carts.product.stock) {
      await axios
        .get(`${API_CART}/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          const req = {
            quantity: carts.product.stock,
          };
          axios
            .put(`${API_CART}/update/${carts.id}`, req, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then(() => {
              getAllCart();
            })
            .catch((err) => {
              console.log(err);
            });
        });
    } else {
      await axios
        .get(`${API_CART}/all`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
        .then(() => {
          const req = {
            quantity: carts.quantity + 1,
          };
          axios
            .put(`${API_CART}/update/${carts.id}`, req, {
              headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
            })
            .then(() => {
              getAllCart();
            })
            .catch((err) => {
              console.log(err);
            });
        });
    }
  };

  const deleteCart = async (carts) => {
    await Swal.fire({
      title: "Ingin menghapus produk ini?",
      text: "Produk ini akan hilang dari keranjang Anda!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Batal",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .get(`${API_CART}/all`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          })
          .then(() => {
            axios
              .delete(`${API_CART}/delete/${carts.id}`, {
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                },
              })
              .then(() => {
                Swal.fire({
                  icon: "success",
                  title: "Berhasil Dihapus!",
                  showConfirmButton: false,
                  timer: 1500,
                });
                getAllCart();
              })
              .catch((err) => {
                console.log(err);
              });
          });
      }
    });
  };
  useEffect(() => {
    getAllCart();
  }, []);

  return (
    <div className="bg-gray-100">
      {/* header start */}
      <div>
        <Navbar />{" "}
      </div>
      {/* header end */}

      {/* content start */}
      <div className="flex justify-center">
        <div className="my-4 max-w-screen-2xl">
          <div className="md:mb-[100px]">
            <div className="hidden md:grid mb-4 grid-cols-6 gap-2 mx-28 py-4 text-gray-400 font-semibold px-2 pl-10 border bg-white rounded shadow">
              <div className="col-span-2">Produk</div>
              <div className="text-center">Harga Satuan</div>
              <div className="text-center">Kuantitas</div>
              <div className="text-center">Total Harga</div>
              <div className="text-center">Aksi</div>
            </div>

            {/* PRODUK UI PC start */}
            <div className="hidden md:block mx-32 px-5">
              {cart.cartItems.length === 0 ? (
                <div className="flex justify-center my-5">
                  <div>
                    <img src={empty} alt="empty-cart" className="w-96 h-72 mb-3" />
                    <p className="text-lg text-center font-bold">
                      Keranjang Anda masih kosong
                    </p>
                    <p className="text-md text-center font-semibold">
                      Kembali ke Beranda untuk memilih barang.
                    </p>
                  </div>
                </div>
              ) : (
                <div className="bg-white rounded shadow border">
                  {cart.cartItems.map((carts) => (
                    <div className="hidden border-b md:grid items-center grid-cols-6 gap-2 py-4 px-2 pl-10 transition duration-300 hover:transition hover:duration-300" key={carts.id}>
                      <div className="col-span-2 flex items-center gap-x-5">
                        <img
                          src={carts.product.image}
                          alt="image_produk"
                          className="w-[80px] h-[80px] border"
                        />
                        <div>
                          <span>{carts.product.name}</span>
                        </div>
                      </div>

                      <div className="text-center">
                        {titik(carts.product.price)}
                      </div>
                      <div className="flex items-center justify-center gap-2">
                        <div className="w-[120px]">
                          <label htmlFor="Quantity" className="sr-only">
                            Quantity
                          </label>

                          <div className="flex items-center border border-gray-300 rounded">
                            <button
                              type="button"
                              onClick={() => decrement(carts)}
                              className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                            >
                              &minus;
                            </button>

                            <input
                              type="number"
                              id="Quantity"
                              value={carts.quantity}
                              className="h-10 w-16 bg-transparent focus:ring-teal-500 border-transparent text-center [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                              autoComplete="off"
                            />

                            <button
                              onClick={() => increment(carts)}
                              type="button"
                              className="w-10 h-10 leading-10 text-gray-600 transition hover:opacity-75"
                            >
                              &#43;
                            </button>
                          </div>
                        </div>
                      </div>
                      <div className="text-center">
                        {titik(carts.product.price * carts.quantity)}
                      </div>
                      <div className="text-center">
                        <span
                          onClick={() => deleteCart(carts)}
                          className="bg-red-100 text-red-700 transition duration-300 hover:transition hover:duration-300 hover:bg-red-700 hover:text-red-50 rounded cursor-pointer px-5 py-2"
                        >
                          Hapus
                        </span>
                      </div>
                    </div>
                  ))}
                </div>
              )}
            </div>
            {/* PRODUK UI PC end */}

            {/* Produk UI Mobile start */}
            <div className="md:hidden mx-0 my-2 p-3">
              {cart.cartItems.length === 0 ? (
                <div className="flex justify-center my-5">
                  <div>
                    <img src={empty} alt="empty-cart" className="w-72 h-52 mb-3" />
                    <p className="text-md text-center font-bold">
                      Keranjang Anda masih kosong
                    </p>
                    <p className="text-sm text-center font-semibold">
                      Kembali ke Beranda untuk memilih barang.
                    </p>
                  </div>
                </div>
              ) : (
                <div className="bg-white rounded shadow border">
                  {cart.cartItems.map((carts) => (
                    <div className="border-b px-2 py-4" key={carts.id}>
                      <div className="flex">
                        <img
                          src={carts.product.image}
                          alt="image_product"
                          className="w-20 h-20"
                        />
                        <div className="w-56 ml-3 leading-7">
                          <p>{carts.product.name}</p>
                          <p>{titik(carts.product.price * carts.quantity)}</p>
                          <div className="flex items-center justify-between gap-2 mt-2">
                            <div className="w-[100px]">
                              <label htmlFor="Quantity" className="sr-only">
                                Quantity
                              </label>

                              <div className="flex items-center border border-gray-300 rounded">
                                <button
                                  type="button"
                                  onClick={() => decrement(carts)}
                                  className="w-10 h-8 leading-3 text-gray-600 transition hover:opacity-75"
                                >
                                  &minus;
                                </button>

                                <input
                                  type="number"
                                  id="Quantity"
                                  value={carts.quantity}
                                  className="h-9 w-16 bg-transparent focus:ring-teal-500 border-transparent text-center [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                                  autoComplete="off"
                                />

                                <button
                                  onClick={() => increment(carts)}
                                  type="button"
                                  className="w-10 h-8 leading-3 text-gray-600 transition hover:opacity-75"
                                >
                                  &#43;
                                </button>
                              </div>
                            </div>
                            <div className="text-center">
                              <span
                                onClick={() => deleteCart(carts)}
                                className="bg-red-100 text-red-700 transition duration-300 hover:transition hover:duration-300 hover:bg-red-700 hover:text-red-50 rounded cursor-pointer px-5 py-2"
                              >
                                Hapus
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              )}
            </div>
            {/* Produk UI Mobile end */}
          </div>

          <div className="sticky px-4 md:py-6 py-3 inset-x-0 bottom-0 md:mx-28 border bg-white rounded-t total">
            <div className="flex items-center justify-between md:justify-end gap-2">
              <div className="gap-2">
                <div className="flex items-center">
                  <p className="mt-0.5">Total</p>
                  <span className="md:ml-2">:</span>
                </div>
                <div>
                  <span className="text-xl font-semibold block md:hidden">
                    {titik(cart.totalCost)}
                  </span>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <div>
                  <span className="text-2xl font-semibold hidden md:block">
                    {titik(cart.totalCost)}
                  </span>
                </div>
                {cart.cartItems.length !== 0 ? (
                  <div>
                    <a href="/checkout">
                      <button
                        type="button"
                        className="inline-block w-full px-7 py-3 bg-[#3ca588] text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-[#4ac4a5] hover:shadow-lg transition duration-150 ease-in-out"
                      >
                        Checkout
                      </button>
                    </a>
                  </div>
                ) : (
                  <>
                    <button
                      disabled
                      type="button"
                      className="inline-block w-full px-7 py-3 bg-[#8bd3c0] text-white font-medium text-sm leading-snug uppercase rounded shadow-md cursor-not-allowed"
                    >
                      Checkout
                    </button>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* content end */}

      {/* footer start */}
      <div className="footer">
        <Footer />
      </div>
      {/* footer end */}
    </div>
  );
}
