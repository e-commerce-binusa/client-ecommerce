import React from "react";
import success from "../assets/image/logo-success.png";

export default function Success() {
  return (
    <div className="flex justify-center items-center h-[100vh] bg-gray-100">
      <div className="text-center">
        <img src={success} alt="logo-success" className="md:h-80 h-72" />
        <h3 className="md:text-2xl text-xl font-semibold">Terimakasih Sudah Memesan!</h3>
        <p className="md:text-xl text-lg">Pesananmu segera diproses</p>
        <div className="flex justify-center my-5">
          <a href="/">
            <button className="bg-[#13ac84] text-white hover:bg-[#5cc4a4] hover:text-gray-800 py-2 px-5 rounded-md hover:transition hover:duration-500 transition duration-500 hover:shadow-md hover:ease-in-out cursor-pointer">
              Kembali
            </button>
          </a>
        </div>
      </div>
    </div>
  );
}
