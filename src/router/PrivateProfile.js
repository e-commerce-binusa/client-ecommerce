import React from "react";
import { Navigate, useLocation } from "react-router-dom";

function PrivateProfile({ children }) {
  const location = useLocation();
  if (localStorage.getItem("token") === null) {
    return <Navigate to="/" state={{ from: location }} />;
  }
  return children;
}

export default PrivateProfile;
