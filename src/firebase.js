// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBNmXuDERbj3hTgLfL4CLiahbAbF5M2xGk",
  authDomain: "ecommerce-binusa-cb31e.firebaseapp.com",
  projectId: "ecommerce-binusa-cb31e",
  storageBucket: "ecommerce-binusa-cb31e.appspot.com",
  messagingSenderId: "302724262547",
  appId: "1:302724262547:web:aefd78288fbc5174163b66"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);
