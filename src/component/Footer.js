import React from "react";
import logo from "../assets/image/logo-dash.png";

export default function Footer() {
  return (
    <div className="bg-white">
      <div className="lg:px-16 md:px-7 px-5 pt-10 pb-5">
        <div className="grid grid-cols-1 md:grid-cols-3 lg:gap-x-5 md:gap-x-1.5 gap-x-0 gap-y-5 mb-7">
          <div className="">
            <div className="lg:block md:block flex justify-center">
              <a href="/">
                <img
                  src={logo}
                  alt="logo-binusa"
                  className="lg:w-56 lg:h-20 md:w-48 md:h-16 w-52 h-20 mb-7"
                />
              </a>
            </div>
            <div className="flex gap-2 mb-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="54"
                height="54"
                fill="currentColor"
                className="bi bi-geo-alt"
                viewBox="0 0 16 16"
              >
                <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z" />
                <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
              </svg>
              <p className="leading-5">
                Jl. Kemantren Raya No.5, RT.02/RW.04, Wonosari, Kec. Ngaliyan,
                Kota Semarang, Jawa Tengah 50186
              </p>
            </div>
            <div className="flex gap-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="23"
                height="23"
                fill="currentColor"
                className="bi bi-telephone"
                viewBox="0 0 16 16"
              >
                <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z" />
              </svg>
              <p>(024) 8662971</p>
            </div>
          </div>
          <div className="grid lg:grid-cols-2 md:grid-cols-3 grid-cols-2 lg:gap-x-5 md:gap-x-1 gap-x-2 ">
            <div className="lg:col-auto md:col-span-2 col-auto ">
              <h4 className="text-md font-medium uppercase mb-2">Partners</h4>
              <ul>
                <li>
                  <a
                    href="https://www.excellentcom.id/"
                    className="hover:underline"
                  >
                    Excellent Computer & Graphics
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.g2academy.co/"
                    className="hover:underline"
                  >
                    G2 Academy
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.intel.co.id/content/www/id/id/education/intel-education.html"
                    className="hover:underline"
                  >
                    Intel Education
                  </a>
                </li>
                <li>
                  <a
                    href="https://mikrotik.com/training/academy"
                    className="hover:underline"
                  >
                    MikroTik Academy
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.telkom.co.id/sites"
                    className="hover:underline"
                  >
                    Telkom Indonesia
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.astra-honda.com/"
                    className="hover:underline"
                  >
                    Astra Honda Motor
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.aoi.co.id/v2/"
                    className="hover:underline"
                  >
                    Apparel One Indonesia
                  </a>
                </li>
              </ul>
            </div>
            <div className="">
              <h4 className="text-md font-medium uppercase">Sosial Media</h4>
              <ul>
                <li>
                  <a
                    href="https://www.instagram.com/smkbinanusantara_smg/"
                    className="hover:underline"
                  >
                    Instagram
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.youtube.com/channel/UCw5YNITJ8UTpWB9N55saWhw"
                    className="hover:underline"
                  >
                    Youtube
                  </a>
                </li>
                <li>
                  <a
                    href="https://binusasmg.sch.id/"
                    className="hover:underline"
                  >
                    Website
                  </a>
                </li>
                <li>
                  <a
                    href="https://twitter.com/smkbinusasmg"
                    className="hover:underline"
                  >
                    Twitter
                  </a>
                </li>
                <li>
                  <a
                    href="https://wa.me/6281391501186"
                    className="hover:underline"
                  >
                    WhatsApp
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="flex justify-center">
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15841.143497621064!2d110.3014482!3d-6.9755592!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x97b3afe1b2104e70!2sSMK%20Bina%20Nusantara%20Semarang!5e0!3m2!1sid!2sid!4v1670421503413!5m2!1sid!2sid"
              className="w-[350px] md:w-full h-56 border-0"
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
              title="SMK BINA NUSANTARA SEMARANG"
            ></iframe>
          </div>
        </div>
        <hr />
        <div className="text-center mt-3">
          <i>
            &copy; 2022{" "}
            <a href="#" className="hover:underline">
              E-Commerce Binusa
            </a>
          </i>
        </div>
      </div>
    </div>
  );
}
