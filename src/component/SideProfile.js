import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { API_USER } from "../util/api";

export default function SideProfile() {
  const [image, setImage] = useState(null);
  const getUser = async () => {
    await axios
      .get(`${API_USER}/list/${localStorage.getItem("userId")}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setImage(res.data.image);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    getUser();
  }, []);
  return (
    <>
      <div className="flex justify-center mb-5">
        <img
          className="w-[100px] h-[100px] rounded-full"
          src={image}
          alt="profile"
        />
      </div>
      <hr className="h-3" />
      <div className="flex items-center">
        <NavLink
          className="cursor-pointer px-5 py-2 w-full rounded"
          to="/profile"
        >
          <span>Profil</span>
        </NavLink>
      </div>
      <div className="flex items-center">
        <NavLink
          className="cursor-pointer px-5 py-2 w-full rounded"
          to="/alamat"
        >
          <span>Alamat</span>
        </NavLink>
      </div>
      <div className="flex items-center">
        <NavLink
          className="cursor-pointer px-5 py-2 w-full rounded"
          to="/ubah-password"
        >
          <span>Ubah Password</span>
        </NavLink>
      </div>
      {localStorage.getItem("role") === "user" ? (
        <div className="flex items-center">
          <NavLink
            className="cursor-pointer px-5 py-2 w-full rounded"
            to="/pesanan"
          >
            <span>Pesanan Saya</span>
          </NavLink>
        </div>
      ) : (
        <></>
      )}
    </>
  );
}
