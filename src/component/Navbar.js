import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import { createSearchParams, useNavigate } from "react-router-dom";
import logo from "../assets/image/binusa-white.png";
import { API_CART, API_USER } from "../util/api";
import "../assets/Navbar.css";

export default function Navbar() {
  const [cart, setCart] = useState({
    cartItems: [],
  });
  const [searchValue, setSearchValue] = useState("");
  const navigate = useNavigate();

  const search = () => {
    return (pathname, params) =>
      navigate(`${pathname}?${createSearchParams(params)}`);
  };

  const navigateSearch = search();

  const goToPosts = () =>
    navigateSearch("/search/product", { name: searchValue });

  let inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setSearchValue(lowerCase);
  };

  const [image, setImage] = useState(null);
  const getUser = async () => {
    await axios
      .get(`${API_USER}/list/${localStorage.getItem("userId")}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        setImage(res.data.image);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getAllCart = async () => {
    await axios
      .get(`${API_CART}/list`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
      .then((res) => {
        if (res.status === 200) {
          setCart(res.data);
        }
      }).catch((err) => {
        console.log(err);
      })
  };

  const logout = () => {
    localStorage.clear();
    navigate("/");
  };

  useEffect(() => {
    getAllCart();
    getUser();
  }, []);
  
  return (
    <div>
      <header
        aria-label="Site Header"
        className="bg-gradient-to-b from-[#13ac84] to-[#5cc4a4] shadow relative"
      >
        <div className="mx-auto max-w-screen-2xl px-4 sm:px-6 lg:px-8">
          <div className="flex h-20 items-center justify-between">
            <div className="hidden md:flex md:items-center md:gap-12">
              <a
                className="flex justify-center items-center text-teal-600 h-16"
                href="/"
              >
                <span className="sr-only">Home</span>
                <img
                  src={logo}
                  alt="logo-binusa"
                  className="md:w-28 md:h-12 lg:w-36 lg:h-14"
                />
              </a>
            </div>

            {/* search input start */}
            <div>
              <form className="w-[150px] md:w-[450px] lg:w-[700px]">
                <label
                  htmlFor="default-search"
                  className="text-sm font-medium text-gray-900 sr-only"
                >
                  Search
                </label>
                <div className="relative md:w-auto w-fit">
                  <input
                    type="search"
                    id="default-search"
                    className="block w-56 md:w-full p-3 text-sm text-gray-900 focus:ring-2 focus:outline-none focus:ring-[#13ac84] rounded-md border-0 bg-white shadow-md"
                    placeholder="Cari disini"
                    autoComplete="off"
                    onChange={inputHandler}
                  />
                  <button
                    onClick={goToPosts}
                    type="submit"
                    className="text-white absolute left-[180px] bottom-2 md:right-2.5 md:left-auto md:bottom-2 bg-[#13ac84] hover:bg-[#5cc4a4] hover:transition hover:duration-500 focus:ring-4 focus:outline-none focus:ring-[#5cc4a4] font-medium rounded-[5px] text-sm px-2 py-1 shadow-md"
                  >
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5 text-white"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                      ></path>
                    </svg>
                  </button>
                </div>
              </form>
            </div>
            {/* search input end */}

            {localStorage.getItem("token") !== null ? (
              <>
                {localStorage.getItem("role") !== "admin" ? (
                  <div className="flex">
                    {/* Profile User Dropdown start */}

                    {/* Cart start */}
                    <a href="/cart" className="text-white relative">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="52"
                        height="52"
                        fill="currentColor"
                        className="bi bi-cart2"
                        viewBox="0 0 30 16"
                      >
                        <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l1.25 5h8.22l1.25-5H3.14zM5 13a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z" />
                      </svg>
                      <div className="inline-flex absolute top-1 right-3 justify-center items-center w-6 h-6 text-xs font-bold text-[#13ac84] bg-white rounded-full border-2 border-[#13ac84]">
                        {cart.cartItems.length}
                      </div>
                    </a>
                    {/* Cart end */}

                    {/* PC UI start */}
                    <div className="hidden md:block">
                      <div className="group inline-block relative">
                        <button className="mt-1 rounded flex justify-center items-center">
                          <img
                            className="w-10 h-10 rounded-full"
                            src={image}
                            alt="avatar"
                          />
                        </button>
                        <div
                          className="absolute hidden text-gray-70 group-hover:block right-0 z-10 mt-1 w-40 origin-top-right rounded-md border border-gray-100 bg-white shadow-lg"
                          role="menu"
                        >
                          <div className="p-2">
                            <a
                              href="/profile"
                              className="block rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Akun Saya
                            </a>
                            <a
                              href="/pesanan"
                              className="block rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Pesanan Saya
                            </a>
                            <button
                              onClick={logout}
                              className="block rounded-lg w-full px-4 py-2 text-left text-sm text-red-700 hover:bg-red-50"
                              role="menuitem"
                            >
                              Log Out
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* PC UI end */}

                    {/* Mobile UI start */}
                    <div className="md:hidden dropdown inline-block relative">
                      <button className="mt-1 rounded flex justify-center items-center">
                        <img
                          className="w-10 h-10 rounded-full"
                          src={image}
                          alt="avatar"
                        />
                      </button>
                      <div
                        className="dropdown-content absolute hidden text-gray-70 group-hover:block right-0 z-10 mt-1 w-40 origin-top-right rounded-md border border-gray-100 bg-white shadow-lg"
                        role="menu"
                      >
                        <div className="p-2">
                          <div className="dropdown">
                            <button
                              className="block rounded-lg w-full text-left px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Akun Saya
                            </button>
                            <ul className="dropdown-content absolute hidden rounded-md border border-gray-100 bg-white shadow-lg text-gray-700 p-2 -ml-40 -mt-10">
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/profile"
                                >
                                  Profile
                                </a>
                              </li>
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/alamat"
                                >
                                  Alamat
                                </a>
                              </li>

                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/ubah-password"
                                >
                                  Ubah Password
                                </a>
                              </li>
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/pesanan"
                                >
                                  Pesanan Saya
                                </a>
                              </li>
                            </ul>
                          </div>
                          <a
                            href="/pesanan"
                            className="md:block hidden  rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                            role="menuitem"
                          >
                            Pesanan Saya
                          </a>
                          <button
                            onClick={logout}
                            className="block rounded-lg w-full px-4 py-2 text-left text-sm text-red-700 hover:bg-red-50"
                            role="menuitem"
                          >
                            Log Out
                          </button>
                        </div>
                      </div>
                    </div>
                    {/* Mobile UI end */}

                    {/* Profile User Dropdown end */}
                  </div>
                ) : (
                  <>
                    {/* Profile Admin Dropdown start */}

                    {/* PC UI start */}
                    <div className="hidden md:block">
                      <div className="group inline-block relative">
                        <button className="mt-1 rounded flex justify-center items-center">
                          <img
                            className="w-10 h-10 rounded-full"
                            src={image}
                            alt="avatar"
                          />
                        </button>
                        <div
                          className="absolute hidden text-gray-70 group-hover:block right-0 z-10 mt-1 w-40 origin-top-right rounded-md border border-gray-100 bg-white shadow-lg"
                          role="menu"
                        >
                          <div className="p-2">
                            <a
                              href="/profile"
                              className="block rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Akun Saya
                            </a>
                            <a
                              href="/admin/dashboard"
                              className="block rounded-lg px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Dashboard
                            </a>
                            <button
                              onClick={logout}
                              className="block rounded-lg w-full px-4 py-2 text-left text-sm text-red-700 hover:bg-red-50"
                              role="menuitem"
                            >
                              Log Out
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* PC UI end */}

                    {/* Mobile UI start */}
                    <div className="md:hidden dropdown inline-block relative">
                      <button className="mt-1 rounded flex justify-center items-center">
                        <img
                          className="w-10 h-10 rounded-full"
                          src={image}
                          alt="avatar"
                        />
                      </button>
                      <div
                        className="dropdown-content absolute hidden text-gray-70 group-hover:block right-0 z-10 mt-1 w-40 origin-top-right rounded-md border border-gray-100 bg-white shadow-lg"
                        role="menu"
                      >
                        <div className="p-2">
                          <div className="dropdown">
                            <button
                              className="block rounded-lg w-full text-left px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                              role="menuitem"
                            >
                              Akun Saya
                            </button>
                            <ul className="dropdown-content absolute hidden rounded-md border border-gray-100 bg-white shadow-lg text-gray-700 p-2 -ml-40 -mt-10">
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/profile"
                                >
                                  Profile
                                </a>
                              </li>
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/alamat"
                                >
                                  Alamat
                                </a>
                              </li>
                              <li>
                                <a
                                  className="hover:bg-gray-50 rounded-lg text-sm py-2 px-4 block whitespace-no-wrap"
                                  href="/ubah-password"
                                >
                                  Ubah Password
                                </a>
                              </li>
                            </ul>
                          </div>
                          <a
                            href="/admin/dashboard"
                            className="block rounded-lg w-full px-4 py-2 text-sm text-gray-500 hover:bg-gray-50 hover:text-gray-700"
                            role="menuitem"
                          >
                            Dashboard
                          </a>
                          <button
                            onClick={logout}
                            className="block rounded-lg w-full px-4 py-2 text-left text-sm text-red-700 hover:bg-red-50"
                            role="menuitem"
                          >
                            Log Out
                          </button>
                        </div>
                      </div>
                    </div>
                    {/* Mobile UI end */}

                    {/* Profile Admin Dropdown end */}
                  </>
                )}
              </>
            ) : (
              <div className="flex items-center gap-4">
                <div className="flex md:gap-3 gap-2">
                  <a
                    className="font-semibold text-white hover:text-gray-300"
                    href="/login"
                  >
                    Login
                  </a>
                  <span className="text-white font-thin">|</span>
                  <div className="">
                    <a
                      className="font-semibold text-white hover:text-gray-300"
                      href="/signup"
                    >
                      Register
                    </a>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </header>
    </div>
  );
}
