import React from "react";
import "../assets/Sidebar.css";
import image from "../assets/image/logo-dash.png";
import { NavLink } from "react-router-dom";

export default function Side() {
  const logout = () => {
    localStorage.clear();
  };
  return (
    <>
      <input type="checkbox" id="menu-open" className="hidden" />

      <header
        className="bg-white text-[#34bc94] flex justify-between md:hidden"
        data-dev-hint="mobile menu bar"
      >
        <a href="#" className="block p-4 font-bold truncate">
          E-Commerce BINUSA
        </a>

        <label
          htmlFor="menu-open"
          id="mobile-menu-button"
          className="m-2 p-2 focus:outline-none hover:text-white hover:bg-[#5cc4a4] rounded-md"
        >
          <svg
            id="menu-open-icon"
            className="h-6 w-6 transition duration-700 ease-in-out"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
          <svg
            id="menu-close-icon"
            className="h-6 w-6 transition duration-700 ease-in-out"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M6 18L18 6M6 6l12 12"
            />
          </svg>
        </label>
      </header>

      <aside
        id="sidebar"
        className="bg-white text-[#34bc94] md:w-64 w-2/4 space-y-6 pt-6 px-0 absolute inset-y-0 left-0 transform md:relative md:translate-x-0 transition duration-700 ease-in-out  md:flex md:flex-col md:justify-between overflow-y-auto h-[100vh]"
        data-dev-hint="sidebar; px-0 for frameless; px-2 for visually inset the navigation"
      >
        <div
          className="flex flex-col space-y-4"
          data-dev-hint="optional div for having an extra footer navigation"
        >
          <a
            href="/"
            className="text-white flex justify-center items-center space-x-2 px-4"
            title="E-Commerce BINUS"
          >
            <img src={image} className="w-44 h-20" alt="BINUSA" />
          </a>
          <hr className="mx-3" />

          <nav data-dev-hint="main navigation">
            <NavLink
              to="/admin/dashboard"
              className="flex items-center space-x-2 py-3 px-4 transition duration-200 hover:bg-[#5cc4a4] hover:text-white"
              activeclassname="active"
            >
              <svg
                className="icon w-5 h-5 fill-current"
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                width="24"
                viewBox="0 0 512 512"
              >
                <path d="M64 64c0-17.7-14.3-32-32-32S0 46.3 0 64V400c0 44.2 35.8 80 80 80H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H80c-8.8 0-16-7.2-16-16V64zm406.6 86.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L320 210.7l-57.4-57.4c-12.5-12.5-32.8-12.5-45.3 0l-112 112c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L240 221.3l57.4 57.4c12.5 12.5 32.8 12.5 45.3 0l128-128z" />
              </svg>
              <span className="font-semibold">Dashboard</span>
            </NavLink>
            <NavLink
              to="/admin/product"
              className="flex items-center space-x-2 py-3 px-4 transition duration-200 hover:bg-[#5cc4a4] hover:text-white"
              activeclassname="active"
            >
              <svg
                className="icon w-5 h-5 fill-current"
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                width="24"
                viewBox="0 0 512 512"
              >
                <path d="M40 48C26.7 48 16 58.7 16 72v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V72c0-13.3-10.7-24-24-24H40zM192 64c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zm0 160c-17.7 0-32 14.3-32 32s14.3 32 32 32H480c17.7 0 32-14.3 32-32s-14.3-32-32-32H192zM16 232v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V232c0-13.3-10.7-24-24-24H40c-13.3 0-24 10.7-24 24zM40 368c-13.3 0-24 10.7-24 24v48c0 13.3 10.7 24 24 24H88c13.3 0 24-10.7 24-24V392c0-13.3-10.7-24-24-24H40z" />
              </svg>
              <span className="font-semibold">Product</span>
            </NavLink>
            <NavLink
              to="/admin/category"
              className="flex items-center space-x-2 py-3 px-4 transition duration-200 hover:bg-[#5cc4a4] hover:text-white"
              activeclassname="active"
            >
              <svg
                className="bi bi-filter-left icon fill-current"
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 16 16"
              >
                <path d="M2 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z" />
              </svg>
              <span className="font-semibold">Category</span>
            </NavLink>
            <NavLink
              to="/admin/transaction"
              className="flex items-center space-x-2 py-3 px-4 transition duration-200 hover:bg-[#5cc4a4] hover:text-white"
              activeclassname="active"
            >
              <svg
                className="icon w-5 h-5 fill-current"
                xmlns="http://www.w3.org/2000/svg"
                height="24"
                width="24"
                viewBox="0 0 512 512"
              >
                <path d="M64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H448c35.3 0 64-28.7 64-64V192c0-35.3-28.7-64-64-64H80c-8.8 0-16-7.2-16-16s7.2-16 16-16H448c17.7 0 32-14.3 32-32s-14.3-32-32-32H64zM416 336c-17.7 0-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32s-14.3 32-32 32z" />
              </svg>
              <span className="font-semibold">Transaksi</span>
            </NavLink>
            <NavLink
              to="/admin/list-user"
              className="flex items-center space-x-2 py-3 px-4 transition duration-200 hover:bg-[#5cc4a4] hover:text-white"
              activeclassname="active"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="icon w-5 h-5 fill-current"
                viewBox="0 0 640 512"
              >
                <path d="M144 160c-44.2 0-80-35.8-80-80S99.8 0 144 0s80 35.8 80 80s-35.8 80-80 80zm368 0c-44.2 0-80-35.8-80-80s35.8-80 80-80s80 35.8 80 80s-35.8 80-80 80zM0 298.7C0 239.8 47.8 192 106.7 192h42.7c15.9 0 31 3.5 44.6 9.7c-1.3 7.2-1.9 14.7-1.9 22.3c0 38.2 16.8 72.5 43.3 96c-.2 0-.4 0-.7 0H21.3C9.6 320 0 310.4 0 298.7zM405.3 320c-.2 0-.4 0-.7 0c26.6-23.5 43.3-57.8 43.3-96c0-7.6-.7-15-1.9-22.3c13.6-6.3 28.7-9.7 44.6-9.7h42.7C592.2 192 640 239.8 640 298.7c0 11.8-9.6 21.3-21.3 21.3H405.3zM416 224c0 53-43 96-96 96s-96-43-96-96s43-96 96-96s96 43 96 96zM128 485.3C128 411.7 187.7 352 261.3 352H378.7C452.3 352 512 411.7 512 485.3c0 14.7-11.9 26.7-26.7 26.7H154.7c-14.7 0-26.7-11.9-26.7-26.7z" />
              </svg>
              <span className="font-semibold">Users</span>
            </NavLink>
          </nav>
        </div>

        <nav data-dev-hint="second-main-navigation or footer navigation">
          <a
            onClick={logout}
            href="/"
            className="flex items-center space-x-2 py-2 px-4 transition duration-200 bottom-0 fixed w-full text-red-500 hover:bg-red-500 hover:text-white"
          >
            <svg
              className="icon w-5 h-5 fill-current"
              xmlns="http://www.w3.org/2000/svg"
              height="24"
              width="24"
              viewBox="0 0 512 512"
            >
              <path d="M160 96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96C43 32 0 75 0 128V384c0 53 43 96 96 96h64c17.7 0 32-14.3 32-32s-14.3-32-32-32H96c-17.7 0-32-14.3-32-32l0-256c0-17.7 14.3-32 32-32h64zM504.5 273.4c4.8-4.5 7.5-10.8 7.5-17.4s-2.7-12.9-7.5-17.4l-144-136c-7-6.6-17.2-8.4-26-4.6s-14.5 12.5-14.5 22v72H192c-17.7 0-32 14.3-32 32l0 64c0 17.7 14.3 32 32 32H320v72c0 9.6 5.7 18.2 14.5 22s19 2 26-4.6l144-136z" />
            </svg>
            <span className="font-semibold">Logout</span>
          </a>
        </nav>
      </aside>
    </>
  );
}
