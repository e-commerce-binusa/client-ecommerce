import { Route, Routes } from "react-router-dom";
import Category from "./pages/admin/Category";
import Dashboard from "./pages/admin/Dashboard";
import Product from "./pages/admin/Product";
import ListUser from "./pages/admin/ListUser";
import Home from "./pages/Home";
import Login from "./pages/Login";
import ProductDetail from "./pages/ProductDetail";
import SignUp from "./pages/SignUp";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import CategoryById from "./pages/CategoryById";
import Profile from "./pages/profile/Profile";
import UbahPassword from "./pages/profile/UbahPassword";
import Alamat from "./pages/profile/Alamat";
import Pesanan from "./pages/profile/Pesanan";
import PrivateRouter from "./router/PrivateRouter";
import Success from "./pages/Success";
import PrivateUser from "./router/PrivateUser";
import Transaction from "./pages/admin/Transaction";
import DetailTransaction from "./pages/admin/DetailTransaction";
import PrivateProfile from "./router/PrivateProfile";
import SearchProduct from "./pages/SearchProduct";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/product/:id" element={<ProductDetail />} />
        <Route path="/search/product" element={<SearchProduct />} />
        <Route
          path="/cart"
          element={
            <PrivateUser>
              <Cart />
            </PrivateUser>
          }
        />
        <Route
          path="/checkout"
          element={
            <PrivateUser>
              <Checkout />
            </PrivateUser>
          }
        />
        <Route path="/category/:id" element={<CategoryById />} />
        <Route
          path="/profile"
          element={
            <PrivateProfile>
              <Profile />
            </PrivateProfile>
          }
        />
        <Route
          path="/ubah-password"
          element={
            <PrivateProfile>
              <UbahPassword />
            </PrivateProfile>
          }
        />
        <Route
          path="/alamat"
          element={
            <PrivateProfile>
              <Alamat />
            </PrivateProfile>
          }
        />
        <Route
          path="/pesanan"
          element={
            <PrivateUser>
              <Pesanan />
            </PrivateUser>
          }
        />
        <Route path="/sukses" element={<Success />} />
        <Route
          path="/admin/dashboard"
          element={
            <PrivateRouter>
              <Dashboard />
            </PrivateRouter>
          }
        />
        <Route
          path="/admin/product"
          element={
            <PrivateRouter>
              <Product />
            </PrivateRouter>
          }
        />
        <Route
          path="/admin/category"
          element={
            <PrivateRouter>
              <Category />
            </PrivateRouter>
          }
        />
        <Route
          path="/admin/transaction"
          element={
            <PrivateRouter>
              <Transaction />
            </PrivateRouter>
          }
        />
        <Route
          path="/admin/pesanan/:id"
          element={
            <PrivateRouter>
              <DetailTransaction />
            </PrivateRouter>
          }
        />
        <Route
          path="/admin/list-user"
          element={
            <PrivateRouter>
              <ListUser />
            </PrivateRouter>
          }
        />
      </Routes>
    </div>
  );
}

export default App;
